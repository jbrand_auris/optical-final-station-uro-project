# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 14:09:40 2016
@author: Yampy
"""

from Acquisition.PhidgetClass import LED
from Acquisition.ZaberStageClass import ZaberStage
from Acquisition.ToshibaClassUro import toshibaImage
from Acquisition.ToshibaSerialCCU import ToshibaCCU
from Acquisition.center_det import get_cent_angle
from Acquisition.PID import PID, sign


import pandas as pd
import numpy as np
import cv2
import time
import os
import shutil
import visa

global led_volt
global uni_volt
global dnr_volt
global edg_volt
#global get extra camera image variagle
global get_extra_image 
#set get_extra_image to True if collecting data from another scope concurrently
get_extra_image = False

led_volt = 1.2  # 300 mA
uni_volt = 3
dnr_volt = 3
edg_volt = 3


class CameraTest:
    #def __init__(self, path, study, datetime, camera_SN, batch, operator, scopeStage, targetStage):
    def __init__(self, path, study, datetime, operator, scopeStage, targetStage):

        self.out2 = []

        while True:
            cont = input("Have all LEDs been switched on and set to Auto Control? Type 'yes' to confirm: ")
            if cont == 'yes':
                break
            else:
                print("Please turn on all LEDs and set the controls to AUTO...\n")

        # Pass path, study and timestamp
        self.path = path
        self.study = study
        self.datetime = datetime
        #self.camera_SN = camera_SN
        self.operator = operator
        #self.batch = batch
        self.out2.append(['attribute', 'value'])
        self.out2.append(['scope', study])
        self.out2.append(['time', datetime])
        #self.out2.append(['toshiba_serial_number', camera_SN])
        #self.out2.append(['batch number', '="'+batch+'"'])
        self.out2.append(['operator', operator])

        # Log default LED voltages
        self.out2.append(['scope_led', led_volt])
        self.out2.append(['uniformity_led', uni_volt])
        self.out2.append(['dynamic_range_led', dnr_volt])
        self.out2.append(['slanted_edge_led', edg_volt])


        # Init LED
        self.led = LED()
        self.led.setVoltage(0, -0.1)
        self.led.setVoltage(1, 5)
        self.led.setVoltage(2, 5)
        self.led.setVoltage(3, 5)
        
        self.scopeStage = scopeStage
        self.targetStage = targetStage
        '''
        # Init Scope Stage 
        self.scopeStage = ZaberStage('COM7', 1510000, 0)  # 1331629 is 0 mm from slanted edge
        self.scopeStage.reset()

        # init Target Stage
        self.targetStage = ZaberStage('COM4', 3000000, 0)
        self.targetStage.reset()
        '''
        # init Thorlabs power meter
        rm = visa.ResourceManager()
        self.thor = rm.open_resource('USB0::0x1313::0x8072::P2008339::INSTR')
        self.thor.write('*RST')
        self.thor.write('SENse:CORRection:WAVelength 535')
        self.out2.append(['thorlabs_wavelength', np.round(float(self.thor.query('SENse:CORRection:WAVelength?')),3)])

        # init Toshiba Image and CCU
        self.cam = toshibaImage()
        self.ccu = ToshibaCCU()

        # Set Auto Shutter Mode (also sets up area for auto control)
        self.ccu.SetShutterMode(0)
        self.out2.append(['toshiba_shutter_mode', self.ccu.GetShutterMode()])
        self.ccu.SetShutterLevel()
        self.out2.append(['toshiba_shutter_level', self.ccu.GetShutterLevel()])
        self.ccu.SetShutterPeakave()
        self.out2.append(['toshiba_shutter_peakave', self.ccu.GetShutterPeakave()])
        self.ccu.SetShutterSpeedAuto()
        self.out2.append(['toshiba_shutter_speed_auto', self.ccu.GetShutterSpeedAuto()])
        self.ccu.SetShutterArea()
        self.out2.append(['toshiba_shutter_area', self.ccu.GetShutterArea()])
        self.ccu.SetCustomArea()
        # How to write custom area/do we care?

        # Set White Balance Status
        self.wb_status = 0

        # Set Manual Gain Mode and Manual Gain to 0
        self.ccu.SetGainMode(1)
        self.out2.append(['toshiba_gain_mode', self.ccu.GetGainMode()])
        self.ccu.SetManGain(0)
        self.out2.append(['toshiba_manual_gain', self.ccu.GetManGain()])

        # Set pedestals!
        self.ccu.SetMped(0)
        self.ccu.SetBped(0)
        self.ccu.SetRped(0)

        # Set WB mode to Manual, Color to 5600K
        self.ccu.SetWbMode(2)
        self.out2.append(['toshiba_wb_mode', self.ccu.GetWbMode()])
        self.ccu.SetWbCtemp(1)
        self.out2.append(['toshiba_wb_ctemp', self.ccu.GetWbCtemp()])

        # Turn Off Gamma
        self.ccu.SetGammaMode(1)
        self.out2.append(['toshiba_gamma_mode', self.ccu.GetGammaMode()])

        # Turn On Edge Enhancement
        self.ccu.SetDTLGain(11)
        self.out2.append(['toshiba_dtl_gain', self.ccu.GetDTLGain()])
        self.ccu.SetDTLFreq(7)
        self.out2.append(['toshiba_dtl_freq', self.ccu.GetDTLFreq()])

        # Disable CCM
        self.ccu.SetMatrix(1)
        self.out2.append(['toshiba_ccm', self.ccu.GetMatrix()])

        # Enable DNR
        self.ccu.SetDNR(2)
        self.out2.append(['toshiba_dnr', self.ccu.GetDNR()])

        # Set Scale to 220x220 (Scale = 1x)
        self.ccu.SetScale(0)
        self.out2.append(['toshiba_scale', self.ccu.GetScale()])

    def reject_outliers(self, data, m=1):
        return np.array(data)[abs(data - np.mean(data)) < m * np.std(data)]

    def mtf_align(self):
        print('\nChecking Scope Alignment...')

        # Turn OFF Edge Enhancement
        self.ccu.SetDTLGain(0)

        # If mtf folder, change cwd into study, else, create and change
        if not os.path.exists(self.path + "\\a_alignment_check"):
            os.makedirs(self.path + "\\a_alignment_check")
        os.chdir(self.path + "\\a_alignment_check")

        # Perform User check to see if Calibration arm was used to position camera
        self.led.setVoltage(1, edg_volt)



        while True:
            cont = input("-(1) Has Scope been positioned in it's natural position correctly? Type 'yes' to confirm: ")
            if cont == 'yes':
                break
            else:
                print("-(1)Please fold the calibration arm and make sure scope is flush with the arm...\n")
        
        while True:
            nat_check = input('Based on the MPI, are either of the Natural Angle Markers rotated greater than or equal to 60 degrees ? (yes/no): ')
            if nat_check == 'yes' or nat_check == 'no':
                break
            else:
                print("Incorrect Entry, are either of the Natural Angle Markers rotated greater than or equal to 60 degrees, as described by the MPI?(yes/no): ")
        
        if nat_check == 'no':
            print("-(2) Proceeding to 0 mm from Slanted Edge Target to check alignment!...")
            # Fix this Value
            self.scopeStage.moveAbsoluteNat(1500000)
            self.cam.avgImage(50)
            self.cam.saveImage('before_alignment_check.bmp')
            if get_extra_image == True:
                input('Capture Extra Image and press enter to continue')
    
            # Perform User check to see if Calibration arm was used to position camera
            while True:
                cont = input(
                    "-(3) Using the manual X and Y knobs on the stages, position the center of the target on the camera? Type 'yes' to confirm: ")
                if cont == 'yes':
                    break
                else:
                    print("-(3) Please position the center of the target onto the center of the camera...\n")
    
            i = 0
            self.scopeStage.moveAbsoluteNat(1000000)
            self.cam.avgImage(50)
            self.cam.saveImage('check_position_' + str(i) + '.bmp')
            if get_extra_image == True:
                input('Capture Extra Image and press enter to continue')
    
            print("\nCollecting natural angle measurement...")
            img = cv2.imread('check_position_' + str(i) + '.bmp')
            img_rev = img[:, ::-1]
            nlin, npix, _ = img.shape
    
            center, angles = get_cent_angle(
                img[int(np.floor(nlin / 4)):int(np.floor(3 * nlin / 4)), int(np.floor(npix / 4)):int(np.floor(3 * npix / 4))])
            center_rev, angles_rev = get_cent_angle(
                img_rev[int(np.floor(nlin / 4)):int(np.floor(3 * nlin / 4)), int(np.floor(npix / 4)):int(np.floor(3 * npix / 4))])
    
            angle = angles[0]
            angle_rev = angles_rev[0]
    
            if angle >= 45 and angle_rev < 45:
                dec_angle = -angle_rev
            elif angle_rev >= 45 and angle < 45:
                dec_angle = angle
            elif angle_rev < 3 and angle < 3:
                dec_angle = 0
            else:
                print(angle, angle_rev)
            print('        -Natural Angles: ' + str(dec_angle-5))
            self.out2.append(['toshiba_natural_angle', dec_angle-5])
    
    
            go = True
    
            while True:
                print("-(4." + str(i) + ") Checking Edge Angle and Center Alignment...")
                img = cv2.imread('check_position_' + str(i) + '.bmp')
                img_rev = img[:, ::-1]
                nlin, npix, _ = img.shape
    
                center, angles = get_cent_angle(
                    img[int(np.floor(nlin / 4)):int(np.floor(3 * nlin / 4)), int(np.floor(npix / 4)):int(np.floor(3 * npix / 4))])
                center_rev, angles_rev = get_cent_angle(
                    img_rev[int(np.floor(nlin / 4)):int(np.floor(3 * nlin / 4)), int(np.floor(npix / 4)):int(np.floor(3 * npix / 4))])
                print('        -Angles: ' + str(angles))
                print('        -Angles Reversed: ' + str(angles_rev))
                angle = angles[0]
                angle_rev = angles_rev[0]
    
                if angle >= 45 and angle_rev < 45:
                    dec_angle = -angle_rev
                elif angle_rev >= 45 and angle < 45:
                    dec_angle = angle
                elif angle_rev < 3 and angle < 3:
                    dec_angle = 0
                else:
                    print(angle, angle_rev)
    
                angle_stat_cw = dec_angle > 10
                angle_stat_ccw = dec_angle < 6
    
                self.out2.append(['toshiba_angle_check_' + str(i), np.round(dec_angle, 3)])
    
                if angle_stat_ccw:
                    self.cam.avgImage(50)
                    self.cam.saveImage('before_alignment_check_' + str(i) + '.bmp')
                    if get_extra_image == True:
                        input('Capture Extra Image and press enter to continue')
                    cont = input("\n-(4." + str(i) + ") Camera rotated too far counter-clockwise, angle is " + str(
                        np.round(dec_angle,
                                 2)) + " degrees,  It needs to be 6-10 degrees, \nplease rotate camera clockwise...\nType 'done' when target is centered: ")
                    print('')
                    i += 1
                    self.cam.avgImage(50)
                    self.cam.saveImage('check_position_' + str(i) + '.bmp')
                    go = False
                elif angle_stat_cw:
                    self.cam.avgImage(50)
                    self.cam.saveImage('before_alignment_check_' + str(i) + '.bmp')
                    if get_extra_image == True:
                        input('Capture Extra Image and press enter to continue')
                    cont = input("\n-(4." + str(i) + ") Camera rotated too far clockwise, angle is " + str(
                        np.round(dec_angle,
                                 2)) + " degrees,  It needs to be 6-10 degrees, \nplease rotate camera counter-clockwise...\nType 'done' when target is centered: ")
                    print('')
                    i += 1
                    self.cam.avgImage(50)
                    self.cam.saveImage('check_position_' + str(i) + '.bmp')
                    if get_extra_image == True:
                        input('Capture Extra Image and press enter to continue')
                    go = False
                else:
                    print('-(4.' + str(i + 1) + ') Camera aligned successfully, angle is at ' + str(
                        np.round(dec_angle, 2)) + ' degrees!')
                    break
    
                    # Perform User check to see if Calibration arm was used to position camera
    
            # Check X/Y again
            if go == False:
                self.scopeStage.moveAbsoluteNat(1500000)
                self.cam.avgImage(50)
                self.cam.saveImage('before_alignment_check_final.bmp')
                if get_extra_image == True:
                    input('Capture Extra Image and press enter to continue')
                while True:
                    cont = input(
                        "-(5) Using the manual X and Y knobs on the stages, position the center of the target on the camera? Type 'yes' to confirm: ")
                    if cont == 'yes':
                        break
                    else:
                        print("-(5) Please position the center of the target onto the center of the camera...\n")
            else:
                shutil.copy(self.path + "\\a_alignment_check\\before_alignment_check.bmp",
                            self.path + "\\a_alignment_check\\before_alignment_check_final.bmp")
    
            # Turn ON Edge Enhancement
            self.ccu.SetDTLGain(11)
    
            self.cam.avgImage(50)
            self.cam.saveImage('after_alignment_check.bmp')
            if get_extra_image == True:
                input('Capture Extra Image and press enter to continue')
    
            # Move Scope Back to 0
            self.scopeStage.reset()
    
            # Turn off slanted edge back illumination
            self.led.setVoltage(1, 5)
            
        if nat_check == 'yes':
            
            self.out2.append(['toshiba_natural_angle', float('NaN')])
            print("-(2) Proceeding to 0 mm from Slanted Edge Target to check alignment!...")
            # Fix this Value
            self.scopeStage.moveAbsoluteNat(1500000)
            self.cam.avgImage(50)
            self.cam.saveImage('before_alignment_check.bmp')
            if get_extra_image == True:
                input('Capture Extra Image and press enter to continue')
    
            # Perform User check to see if Calibration arm was used to position camera
            while True:
                cont = input(
                    "-(3) Using the manual X and Y knobs on the stages, position the center of the target on the camera? Type 'yes' to confirm: ")
                if cont == 'yes':
                    break
                else:
                    print("-(3) Please position the center of the target onto the center of the camera...\n")
    
            i = 0
            self.scopeStage.moveAbsoluteNat(1000000)
            self.cam.avgImage(50)
            self.cam.saveImage('check_position_' + str(i) + '.bmp')
            if get_extra_image == True:
                input('Capture Extra Image and press enter to continue')
    
            print("\nCollecting angle measurement...")
            img = cv2.imread('check_position_' + str(i) + '.bmp')
            img_rev = img[:, ::-1]
            nlin, npix, _ = img.shape
    
            center, angles = get_cent_angle(
                img[int(np.floor(nlin / 4)):int(np.floor(3 * nlin / 4)), int(np.floor(npix / 4)):int(np.floor(3 * npix / 4))])
            center_rev, angles_rev = get_cent_angle(
                img_rev[int(np.floor(nlin / 4)):int(np.floor(3 * nlin / 4)), int(np.floor(npix / 4)):int(np.floor(3 * npix / 4))])
    
            angle = angles[0]
            angle_rev = angles_rev[0]
    
            if angle >= 45 and angle_rev < 45:
                dec_angle = -angle_rev
            elif angle_rev >= 45 and angle < 45:
                dec_angle = angle
            elif angle_rev < 3 and angle < 3:
                dec_angle = 0
            else:
                print(angle, angle_rev)
    
    
            go = True
    
            while True:
                print("-(4." + str(i) + ") Checking Edge Angle and Center Alignment...")
                img = cv2.imread('check_position_' + str(i) + '.bmp')
                img_rev = img[:, ::-1]
                nlin, npix, _ = img.shape
    
                center, angles = get_cent_angle(
                    img[int(np.floor(nlin / 4)):int(np.floor(3 * nlin / 4)), int(np.floor(npix / 4)):int(np.floor(3 * npix / 4))])
                center_rev, angles_rev = get_cent_angle(
                    img_rev[int(np.floor(nlin / 4)):int(np.floor(3 * nlin / 4)), int(np.floor(npix / 4)):int(np.floor(3 * npix / 4))])
                print('        -Angles: ' + str(angles))
                print('        -Angles Reversed: ' + str(angles_rev))
                angle = angles[0]
                angle_rev = angles_rev[0]
    
                if angle >= 45 and angle_rev < 45:
                    dec_angle = -angle_rev
                elif angle_rev >= 45 and angle < 45:
                    dec_angle = angle
                elif angle_rev < 3 and angle < 3:
                    dec_angle = 0
                else:
                    print(angle, angle_rev)
    
                angle_stat_cw = dec_angle > 10
                angle_stat_ccw = dec_angle < 6
    
                self.out2.append(['toshiba_angle_check_' + str(i), np.round(dec_angle, 3)])
    
                if angle_stat_ccw:
                    self.cam.avgImage(50)
                    self.cam.saveImage('before_alignment_check_' + str(i) + '.bmp')
                    if get_extra_image == True:
                        input('Capture Extra Image and press enter to continue')
                    cont = input("\n-(4." + str(i) + ") Camera rotated too far counter-clockwise, angle is " + str(
                        np.round(dec_angle,
                                 2)) + " degrees,  It needs to be 6-10 degrees, \nplease rotate camera clockwise...\nType 'done' when target is centered: ")
                    print('')
                    i += 1
                    self.cam.avgImage(50)
                    self.cam.saveImage('check_position_' + str(i) + '.bmp')
                    go = False
                elif angle_stat_cw:
                    self.cam.avgImage(50)
                    self.cam.saveImage('before_alignment_check_' + str(i) + '.bmp')
                    if get_extra_image == True:
                        input('Capture Extra Image and press enter to continue')
                    cont = input("\n-(4." + str(i) + ") Camera rotated too far clockwise, angle is " + str(
                        np.round(dec_angle,
                                 2)) + " degrees,  It needs to be 6-10 degrees, \nplease rotate camera counter-clockwise...\nType 'done' when target is centered: ")
                    print('')
                    i += 1
                    self.cam.avgImage(50)
                    self.cam.saveImage('check_position_' + str(i) + '.bmp')
                    if get_extra_image == True:
                        input('Capture Extra Image and press enter to continue')
                    go = False
                else:
                    print('-(4.' + str(i + 1) + ') Camera aligned successfully, angle is at ' + str(
                        np.round(dec_angle, 2)) + ' degrees!')
                    break
    
                    # Perform User check to see if Calibration arm was used to position camera
    
            # Check X/Y again
            if go == False:
                self.scopeStage.moveAbsoluteNat(1500000)
                self.cam.avgImage(50)
                self.cam.saveImage('before_alignment_check_final.bmp')
                if get_extra_image == True:
                    input('Capture Extra Image and press enter to continue')
                while True:
                    cont = input(
                        "-(5) Using the manual X and Y knobs on the stages, position the center of the target on the camera? Type 'yes' to confirm: ")
                    if cont == 'yes':
                        break
                    else:
                        print("-(5) Please position the center of the target onto the center of the camera...\n")
            else:
                shutil.copy(self.path + "\\a_alignment_check\\before_alignment_check.bmp",
                            self.path + "\\a_alignment_check\\before_alignment_check_final.bmp")
    
            # Turn ON Edge Enhancement
            self.ccu.SetDTLGain(11)
    
            self.cam.avgImage(50)
            self.cam.saveImage('after_alignment_check.bmp')
            if get_extra_image == True:
                input('Capture Extra Image and press enter to continue')
    
            # Move Scope Back to 0
            self.scopeStage.reset()
    
            # Turn off slanted edge back illumination
            self.led.setVoltage(1, 5)
        

    def white_balance(self):

        # LED on Colortile
        self.led.setVoltage(0, led_volt)

        # If Color folder, change cwd into study, else, create and change
        if not os.path.exists(self.path + "\\b_white_balance"):
            os.makedirs(self.path + "\\b_white_balance")
        os.chdir(self.path + "\\b_white_balance")

        print("\nPositioning Toshiba camera for White Balance onto Uniformity Target...")
        self.targetStage.moveAbsoluteNat(1430000)
        self.scopeStage.moveAbsoluteNat(1050000)

        print("-(1) Saving Pre MWB Picture...")
        self.cam.avgImage(50)
        self.cam.saveImage('before_mwb.bmp')
        if get_extra_image == True:
            input('Capture Extra Image and press enter to continue')

        print("-(2) Performing Manual White Balance Correction...")
        if self.ccu.GetData(102) == 255:
            self.rgain_before = -self.ccu.GetData(103)
        else:
            self.rgain_before = self.ccu.GetData(103)
        self.out2.append(['toshiba_rgain_before', self.rgain_before])

        if self.ccu.GetData(104) == 255:
            self.bgain_before = -self.ccu.GetData(105)
        else:
            self.bgain_before = self.ccu.GetData(105)
        self.out2.append(['toshiba_bgain_before', self.bgain_before])

        i = 1
        pR = PID(2.0, 0.6, 1.0)
        pB = PID(2.0, 0.6, 1.0)
        pR.setPoint(0)
        pB.setPoint(0)

        minR = 99
        minB = 99
        minRval = 0
        minBval = 0

        # Give a way off start point
        self.ccu.SetWbRgain(-50)
        self.ccu.SetWbBgain(50)

        while True:
            rgb = np.mean(np.mean(self.cam.getImage()[:, :, ::-1], axis=0), axis=0)
            # Wikipedia YCbCr Definition of Luminescence
            ry = 0.7874 * rgb[0] - .7152 * rgb[1] - .0722 * rgb[2]
            by = -.2126 * rgb[0] - .7152 * rgb[1] + .9278 * rgb[2]
            print(' -Trial ' + str(i) + ': R-Y: ' + str(np.round(ry, 2)) + ' B-Y: ' + str(np.round(by, 2)))

            rgain = sign(self.ccu.GetWbRgain())
            bgain = sign(self.ccu.GetWbBgain())

            if abs(ry) + abs(by) < abs(minR) + abs(minB):
                minR = ry
                minB = by
                minRval = rgain
                minBval = bgain

            if abs(ry) < 0.25 and abs(by) < 0.25:
                self.wb_status = 1
                self.out2.append(['toshiba_wb_status', self.wb_status])
                self.out2.append(['toshiba_wb_check_ry', ry])
                self.out2.append(['toshiba_wb_check_by', by])
                break

            if i > 500:
                print('White Balance Failed to converge, closest values are;')
                self.ccu.SetWbRgain(minRval)
                self.ccu.SetWbBgain(minBval)
                rgain = minRval
                bgain = minBval
                self.out2.append(['toshiba_wb_status', self.wb_status])
                self.out2.append(['toshiba_wb_check_ry', ry])
                self.out2.append(['toshiba_wb_check_by', by])
                break

            pidR = pR.update(ry)
            rgain += 0.05 * pidR
            rgain = np.int(rgain)
            pidB = pB.update(by)
            bgain += 0.05 * pidB
            bgain = np.int(bgain)
            # print ('PidR: '+str(pidR)+' PidB: '+str(pidB)+'\n')

            self.ccu.SetWbRgain(rgain)
            self.ccu.SetWbBgain(bgain)
            i += 1

        print('\n -RESULTS:')
        print(' -Manual RGB: ' + str(rgb))
        print(' -RG: ' + str(rgain) + ' BG: ' + str(bgain) + '\n')
        self.out2.append(['toshiba_wb_manual_r', rgb[0]])
        self.out2.append(['toshiba_wb_manual_g', rgb[1]])
        self.out2.append(['toshiba_wb_manual_b', rgb[2]])

        print("-(3) Performing Auto White Balance Correction (for comparison)...")
        self.rgain_after = rgain
        self.bgain_after = bgain
        
        self.out2.append(['toshiba_rgain_after', rgain])
        self.out2.append(['toshiba_bgain_after', bgain])


        self.ccu.SetWbMode(0)
        self.ccu.SetDoAwb()
        time.sleep(2)
        self.awbR = 9999  # self.ccu.GetWbRread()
        self.awbB = 9999  # self.ccu.GetWbBread()

        print("-(4) Saving Post AWB Picture...\n")
        self.cam.avgImage(50)
        self.cam.saveImage('after_awb.bmp')
        if get_extra_image == True:
            input('Capture Extra Image and press enter to continue')

        rgb = np.mean(np.mean(self.cam.getImage()[:, :, ::-1], axis=0), axis=0)
        print(' -Auto RGB: ' + str(rgb) + '\n')
        self.out2.append(['toshiba_wb_auto_r', rgb[0]])
        self.out2.append(['toshiba_wb_auto_g', rgb[1]])
        self.out2.append(['toshiba_wb_auto_b', rgb[2]])
        self.ccu.SetWbMode(2)

        print("-(4) Saving Post MWB Picture...\n")
        self.cam.avgImage(50)
        self.cam.saveImage('after_mwb.bmp')
        if get_extra_image == True:
            input('Capture Extra Image and press enter to continue')

        print("-(5) White Balance complete, proceeding to uniformity measurements...")
        # Test Simplication to speed things up
        '''
        self.scopeStage.reset()
        self.targetStage.reset()
        '''

        # LED on colortile to turn off to be ready for next step the slanted edge
        self.led.setVoltage(0, 0)

    def uniformity(self):
        global uni_volt

        # LED on Uniformity
        self.led.setVoltage(3, uni_volt)

        # If Uniformity folder, change cwd into study, else, create and change
        if not os.path.exists(self.path + "\\g_uniformity"):
            os.makedirs(self.path + "\\g_uniformity")
        os.chdir(self.path + "\\g_uniformity")

        print("\nStarting Uniformity Test...")
        # Test Speedup
        '''
        self.targetStage.moveAbsoluteNat(2934000)
        self.scopeStage.moveAbsoluteNat(900000)
        '''

        # Turn Off Scope and Uniformity LED
        self.led.setVoltage(3, 5)
        self.led.setVoltage(0, -0.1)
        time.sleep(0.1)

        print("-(1) Setting Up Master Pedestal...")
        self.mPedSet = -200
        self.ccu.SetMped(self.mPedSet)

        print("-(2) Grabbing Dark Image...")
        while True:
            self.cam.avgImage(10)
            self.cam.saveImage('uniformity_dark_image.bmp')
            result = self.checkImageBlack('uniformity_dark_image.bmp')
            if result[0] == 0 and not self.mPedSet == 200:
                self.mPedSet += 2
                self.ccu.SetMped(self.mPedSet)
                print(' -Master Pedestal Set to ' + str(self.mPedSet))
            elif result[0] == 0 and self.mPedSet == 200:
                print('ERROR: Master Pedestal Range Maxed Out, Something is wrong!\n')
                break
            else:
                break

            
        if get_extra_image == True:
            input('Capture Extra Image and press enter to continue')
        self.out2.append(['toshiba_master_pedestal', self.mPedSet])
        self.cam.avgImage(50)
        self.cam.saveImage('uniformity_dark_image.bmp')
        if get_extra_image == True:
            input('Capture Extra Image and press enter to continue')

        print("\n-(3) Checking LED Illumination Uniformity...")
        # Max Scope LED Power
        volt = led_volt
        self.led.setVoltage(0, volt)
        print('-Scope LED at Max Power (700mA)')
        time.sleep(0.1)

        while True:
            self.cam.avgImage(10)
            self.cam.saveImage('uniformity_led.bmp')
            if get_extra_image == True:
                input('Capture Extra Image and press enter to continue')
            result = self.checkImageSat('uniformity_led.bmp')
            if result[0] == 0:
                print(' -Decreasing Voltage on Scope LED to ' + str(np.round(volt, 2)) + ' Volts')
                volt -= 0.01
                self.led.setVoltage(0, volt)
                # Mandatory Sleep to let Phidget catch up with command
                time.sleep(0.1)
            else:
                print(' -Scope LED Voltage successfully adjusted, moving forward')
                break

        self.out2.append(['toshiba_scope_led_uniformity', volt])
        self.cam.avgImage(50)
        self.cam.saveImage('uniformity_led.bmp')
        if get_extra_image == True:
            input('Capture Extra Image and press enter to continue')

        # Turn Off Scope LED
        self.led.setVoltage(0, -0.1)

        print("\n-(4) Checking Vingetting...")
        # Max Uniformity LED
        volt = 0
        self.led.setVoltage(3, volt)
        time.sleep(0.1)

        while True:
            self.cam.avgImage(10)
            self.cam.saveImage('uniformity_camera.bmp')
            if get_extra_image == True:
                input('Capture Extra Image and press enter to continue')
            result = self.checkImageSat('uniformity_camera.bmp')
            if result[0] == 0:
                print(' -Decreasing Voltage on Unformity LED to ' + str(np.round(volt, 2)) + ' Volts')
                volt += 0.01
                self.led.setVoltage(3, volt)
                # Mandatory Sleep to let Phidget catch up with command
                time.sleep(0.1)
            else:
                print(' -Unformity LED Voltage successfully adjusted, moving forward')
                break

        self.out2.append(['toshiba_target_led_uniformity', volt])
        self.cam.avgImage(50)
        self.cam.saveImage('uniformity_camera.bmp')
        if get_extra_image == True:
            input('Capture Extra Image and press enter to continue')

        print("\n-(5) Grabbing Gain = 10 Image for Fixed Pattern Noise...")
        # Max Uniformity LED
        volt = 3.5
        self.led.setVoltage(3, volt)
        self.ccu.SetManGain(10)
        time.sleep(0.1)

        while True:
            self.cam.avgImage(10)
            self.cam.saveImage('fpn.bmp')
            if get_extra_image == True:
                input('Capture Extra Image and press enter to continue')
            result = self.checkImageSat('fpn.bmp')
            if result[0] == 0:
                print('Decreasing Voltage on Unformity LED to ' + str(np.round(volt, 2)) + ' Volts')
                volt += 0.01
                self.led.setVoltage(3, volt)
                # Mandatory Sleep to let Phidget catch up with command
                time.sleep(0.1)
            else:
                print('Unformity LED Voltage successfully adjusted, moving forward')
                break

        self.out2.append(['toshiba_target_led_uniformity_fpn', volt])
        self.cam.avgImage(50)
        self.cam.saveImage('fpn.bmp')
        if get_extra_image == True:
            input('Capture Extra Image and press enter to continue')
        self.ccu.SetManGain(0)
        
        self.led.setVoltage(3, 5)
        # Turn on LED
        self.led.setVoltage(0, led_volt)
        
        #Capture FOV Images
        # If FOV folder, change cwd into study, else, create and change
        if not os.path.exists(self.path + "\\k_field_of_view_image"):
            os.makedirs(self.path + "\\k_field_of_view_image")
        os.chdir(self.path + "\\k_field_of_view_image")
        
        print("- Capturing Ring Target Image for FOV...")
        self.scopeStage.moveAbsoluteNat(550000)
        self.cam.avgImage(150)
        self.cam.saveImage('ring_target_1.bmp')
        
        self.scopeStage.moveAbsoluteNat(500000)
        self.cam.avgImage(150)
        self.cam.saveImage('ring_target_2.bmp')
        
        self.scopeStage.moveAbsoluteNat(400000)
        self.cam.avgImage(150)
        self.cam.saveImage('ring_target_3.bmp')
        
        self.scopeStage.moveAbsoluteNat(200000)
        self.cam.avgImage(150)
        self.cam.saveImage('ring_target_4.bmp')
        
        print("\n-(6) Uniformity Test image and FOV collection complete, resetting Target Stage...")
        self.scopeStage.reset()
        self.targetStage.reset()

    def farField(self):
        print('\nTaking Far Field Image with LED ON...')

        # If mtf folder, change cwd into study, else, create and change
        if not os.path.exists(self.path + "\\h_far_field"):
            os.makedirs(self.path + "\\h_far_field")
        os.chdir(self.path + "\\h_far_field")

        # reset Stages
        self.scopeStage.reset()
        self.targetStage.reset()

        # Turn On LED
        self.led.setVoltage(0, led_volt)

        # Grab Image
        self.cam.avgImage(50)
        self.cam.saveImage('far_field_led_on.bmp')
        if get_extra_image == True:
            input('Capture Extra Image and press enter to continue')

        # Turn Off LED
        self.led.setVoltage(0, -0.1)

    def get_mtf_data(self):
        number_of_averages = 150
        print("\nStarting MTF Image Collection...")

        # If mtf folder, change cwd into study, else, create and change
        if not os.path.exists(self.path + "\\c_mtf"):
            os.makedirs(self.path + "\\c_mtf")
        os.chdir(self.path + "\\c_mtf")

        # Turn on slanted edge back illumination
        self.led.setVoltage(1, edg_volt)

        # Use Soft Edges on Camera for MTF Stack
        print('-(1) Switching OFF Edge Enhancement...')
        self.ccu.SetDTLGain(0)
        self.ccu.SetDTLFreq(1)

        print("-(2) Proceeding to 0 mm from Slanted Edge Target...")
        self.scopeStage.moveAbsoluteNat(1510000)  # an averaged loc based on six 0 mm manual location finding

        self.cam.avgImage(number_of_averages)
        self.cam.saveImage('00.0_mm.bmp')

        scopeStagePosMM = self.scopeStage.getPosition()
        distances = np.concatenate((np.arange(0.5, 10, 0.5), np.arange(10, 52, 4)))

        positions = len(distances)
        for i in range(len(distances)):
            print("-(" + str(i + 3) + ") Proceeding to " + str(distances[i]) + " mm from Slanted Edge target...")
            new_position = scopeStagePosMM - distances[i]
            self.scopeStage.moveAbsolute(new_position)

            # Insert Picture Taking Routine, average of 10 images
            save_string = str(distances[i])
            if len(save_string) == 3:
                save_string = '0' + save_string
            self.cam.avgImage(number_of_averages)
            self.cam.saveImage(save_string + '_mm.bmp')
            if get_extra_image == True:
                input('Capture Extra Image and press enter to continue')

        print('-(' + str(positions + 3) + ') Switching ON Edge Enhancement...')
        self.ccu.SetDTLGain(11)
        self.ccu.SetDTLFreq(7)

        # Turn off slanted edge back illumination
        self.led.setVoltage(1, 5)

        print("-(" + str(positions + 4) + ") Slanted Edge image collection complete, Proceeding to Dynamic Range")
        self.scopeStage.reset()

    def DNR(self):

        # LED on DNR
        self.led.setVoltage(2, dnr_volt)

        # If DNR folder, change cwd into study, else, create and change
        if not os.path.exists(self.path + "\\d_dynamic_range"):
            os.makedirs(self.path + "\\d_dynamic_range")
        os.chdir(self.path + "\\d_dynamic_range")

        print("\nStarting Dynamic Range test...")
        self.scopeStage.moveAbsoluteNat(300000)
        self.targetStage.moveAbsoluteNat(1900000)

        # Insert Camera Capture Here!
        self.cam.avgImage(50)
        self.cam.saveImage('dynamic_range.bmp')
        if get_extra_image == True:
            input('Capture Extra Image and press enter to continue')

        print("-(1) DNR image collection complete, Proceeding to Distortion...")
        # self.scopeStage.reset()
        # self.targetStage.reset()

    def Distortion(self):

        # If Distortion folder, change cwd into study, else, create and change
        if not os.path.exists(self.path + "\\i_distortion"):
            os.makedirs(self.path + "\\i_distortion")
        os.chdir(self.path + "\\i_distortion")

        print("\nStarting Distortion test...")
        self.scopeStage.moveAbsoluteNat(700000)
        self.targetStage.moveAbsoluteNat(2030000)

        # Insert Camera Capture Here!
        self.cam.avgImage(50)
        self.cam.saveImage('distortion.bmp')
        if get_extra_image == True:
            input('Capture Extra Image and press enter to continue')

        print("-(1) Distortion image collection complete, Proceeding to Veiling Glare...")
        # self.scopeStage.reset()
        # self.targetStage.reset()
        self.led.setVoltage(2, 5)

    def veilingGlare(self):

        # LED OFF Veiling
        self.led.setVoltage(0, led_volt)

        # If Glare folder, change cwd into study, else, create and change
        if not os.path.exists(self.path + "\\e_glare"):
            os.makedirs(self.path + "\\e_glare")
        os.chdir(self.path + "\\e_glare")

        print("\nStarting Veiling Glare test...")
        self.targetStage.moveAbsoluteNat(2340000)
        self.scopeStage.moveAbsoluteNat(650000)

        print("-(1) Capturing(copying) Dark Image")
        shutil.copy(self.path + "\\g_uniformity\\uniformity_dark_image.bmp",
                    self.path + "\\e_glare\\glare_dark_image.bmp")

        print("-(2) Capturing Lit Image")
        # Max Scope LED Power
        volt = 0.6
        self.led.setVoltage(0, volt)
        time.sleep(0.1)

        while True:
            self.cam.avgImage(10)
            self.cam.saveImage('glare_image.bmp')
            if get_extra_image == True:
                input('Capture Extra Image and press enter to continue')
            result = self.checkImageSat('glare_image.bmp')
            if result == 0:
                print('  -Decreasing Voltage on Scope LED to ' + str(np.round(volt, 2)) + ' Volts')
                volt -= 0.01
                self.led.setVoltage(0, volt)
                # Mandatory Sleep to let Phidget catch up with command
                time.sleep(0.1)
            else:
                print('-(3)LED Scope Voltage successfully adjusted, moving forward')
                break

        self.out2.append(['toshiba_scope_led_veiling', volt])
        self.cam.avgImage(50)
        self.cam.saveImage('glare_image.bmp')

        print("-(4) Veiling Glare image collection complete, proceeding to Color Test...")
        # self.scopeStage.reset()
        # self.targetStage.reset()

    def Clocking(self):

        # If Clocking folder, change cwd into study, else, create and change
        if not os.path.exists(self.path + "\\j_clocking"):
            os.makedirs(self.path + "\\j_clocking")
        os.chdir(self.path + "\\j_clocking")

        print("\nStarting Clocking Test...")
        self.targetStage.moveAbsoluteNat(2720000)
        self.scopeStage.moveAbsoluteNat(680000)

        # Insert Camera Capture Here!
        self.cam.avgImage(50)
        self.cam.saveImage('clocking.bmp')
        if get_extra_image == True:
            input('Capture Extra Image and press enter to continue')

        print("-(1) Clocking image collection complete, resetting Scope Stage...")
        # self.scopeStage.reset()
        # self.targetStage.reset()

        # Turn Off Scope LED
        self.led.setVoltage(0, -0.1)

    def colorTest(self):

        # LED on Veiling
        self.led.setVoltage(0, led_volt)

        # If Color folder, change cwd into study, else, create and change
        if not os.path.exists(self.path + "\\f_color"):
            os.makedirs(self.path + "\\f_color")
        os.chdir(self.path + "\\f_color")

        print("\nStarting Color Test...")
        self.targetStage.moveAbsoluteNat(2850000)
        self.scopeStage.moveAbsoluteNat(580000)

        # Insert Camera Capture Here!
        self.cam.avgImage(50)
        self.cam.saveImage('color.bmp')
        if get_extra_image == True:
            input('Capture Extra Image and press enter to continue')

        print("-(1) Color Test image collection complete...")
        # self.scopeStage.reset()
        # self.targetStage.reset()

        # Turn Off Scope LED
        self.led.setVoltage(0, -0.1)

    def measPower(self):
        print("\nPositioning Toshiba camera to measure LED power output...")
        self.targetStage.moveAbsoluteNat(2970000)
        self.scopeStage.moveAbsoluteNat(811559)

        # Turn On LED
        self.led.setVoltage(0, led_volt)
        time.sleep(2.0)
        self.silicon = np.round(float(self.thor.query('READ?'))*1000, 2)
        self.out2.append(['led_scope_power_mw', self.silicon])

        # Turn Off LED
        self.led.setVoltage(0, -0.1)

        print("-(1) Power Test collection complete, "+str(self.silicon)+" mW.  Resetting Scope Stage...")
        self.scopeStage.reset()
        self.targetStage.reset()

    def checkImageSat(self, file):

        img = cv2.imread(file)

        if len(img.shape) == 3:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        meanp = np.mean(img)
        percentSat = len(np.where(img == 255)[0]) / len(img) / len(img[0])
        print('   -CHECK: ' + str(
            np.round(100 * percentSat, 2)) + '% of pixels are Saturated, mean pixel count is ' + str(meanp))
        if percentSat > 0.05:
            return [0, percentSat]
        else:
            return [1, percentSat]

    def checkImageBlack(self, file):

        img = cv2.imread(file)
        if len(img.shape) == 3:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        percentZero = len(np.where(img == 0)[0]) / len(img) / len(img[0])
        print('   -CHECK: ' + str(np.round(100 * percentZero, 2)) + '% of pixels are at 0 counts')
        if percentZero > 0.05:
            return [0, percentZero]
        else:
            return [1, percentZero]

    def saveData(self):

        os.chdir(self.path)
        Data2 = pd.DataFrame(self.out2)
        headers = Data2.iloc[0]
        Data2 = Data2[1:]
        Data2.rename(columns = headers, inplace = True)
        Data2.to_csv(self.study+'_'+self.datetime+'_'+'output.csv', index=False, na_rep = "NULL")
        '''
        entry = ['Manual R Gain Before Test', 'Manual B Gain Before Test', 'Manual R Gain After Test',
                 'Manual B Gain After Test', 'Auto R Gain', 'Auto B Gain', 'Master Pedestal', 'Scope Voltage',
                 'Uniformity Voltage', 'Dynamic Range Voltage', 'Slanted Edge Voltage', 'LED Scope Power (mW)',
                 'WB Status']
        data = [self.rgain_before, self.bgain_before, self.rgain_after, self.bgain_after, self.awbR, self.awbB,
                self.mPedSet, led_volt, uni_volt, dnr_volt, edg_volt, self.silicon, self.wb_status]
        Data = pd.DataFrame({'(B) Data': data, '(A) Entry': entry})
        Data.to_csv('Camera_data.csv', index=False)
        '''


    def writeDataToJointBoard(self):
        # Write Master Pedestal to Joint Board Address 100-101, if negative Data(H) = 255, else 0
        if self.mPedSet < 0:
            self.ccu.SetData(100, 255)
        else:
            self.ccu.SetData(100, 0)
            # Write Pedestal to 101
        self.ccu.SetData(101, self.mPedSet)

        # Write Red Gain to Joint Board Address 102-103, if Gain is negative data @ 102 = 255 else 0
        if self.rgain_after < 0:
            self.ccu.SetData(102, 255)
        else:
            self.ccu.SetData(102, 0)
            # Write Manual R Gain to 103
        self.ccu.SetData(103, self.rgain_after)

        # Write Blue Gain to Joint Board Address 104-105, if Gain is negative data @ 104 = 255 else 0
        if self.bgain_after < 0:
            self.ccu.SetData(104, 255)
        else:
            self.ccu.SetData(104, 0)
            # Write Manual B Gain to 103
        self.ccu.SetData(105, self.bgain_after)

    def close(self):

        # Close Scope Stage
        self.scopeStage.close()

        # Close Target Stage
        self.targetStage.close()

        # Close CCU Connection
        self.ccu.close()

        # Close Thorlabs Connection
        self.thor.close()

    def __del__(self):
        self.close()
