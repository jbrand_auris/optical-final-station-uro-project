# -*- coding: utf-8 -*-
"""
Created on Tue Sep 19 16:20:02 2017

@author: Jonathan.Brand
"""
import numpy as np
from ToshibaSerialCCU import ToshibaCCU

print('Display the current Joint board Settings od a camera')



ccu = ToshibaCCU()
ccu.SetWbMode(2)
if ccu.GetData(102) == 255:
    rgain_before = -1*(256-ccu.GetData(103))
else:
    rgain_before = ccu.GetData(103)
print('R gain set to '+str(rgain_before))

if ccu.GetData(104) == 255:
    bgain_before = -1*(256-ccu.GetData(105))
else:
    bgain_before = ccu.GetData(105)
print('B gain set to '+str(bgain_before))

if ccu.GetData(100) == 255:
    mped = -1*(256-ccu.GetData(101))
else:
    mped = ccu.GetData(101)
print('Master pedestal gain set to '+str(mped))

ccu.close()