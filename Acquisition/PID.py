# -*- coding: utf-8 -*-
"""
Created on Thu Dec 15 14:48:47 2016

@author: steven.yampolsky
"""

#The recipe gives simple implementation of a Discrete Proportional-Integral-Derivative (PID) controller. PID controller gives output value for error between desired reference input and measurement feedback to minimize error value.
#More information: http://en.wikipedia.org/wiki/PID_controller
#
#cnr437@gmail.com
#
#######	Example	#########
#
#p=PID(3.0,0.4,1.2)
#p.setPoint(5.0)
#while True:
#     pid = p.update(measurement_value)
#
#
class PID:
    """
    Discrete PID control
    """

    def __init__(self, P=2.0, I=0.0, D=1.0, Derivator=0, Integrator=0, Integrator_max=500, Integrator_min=-500):

        self.Kp=P
        self.Ki=I
        self.Kd=D
        self.Derivator=Derivator
        self.Integrator=Integrator
        self.Integrator_max=Integrator_max
        self.Integrator_min=Integrator_min
        self.set_point=0.0
        self.error=0.0

     

    def update(self,current_value):
        """
        Calculate PID output value for given reference input and feedback
        """

        self.error = self.set_point - current_value

        self.P_value = self.Kp * self.error
        self.D_value = self.Kd * ( self.error - self.Derivator)
        self.Derivator = self.error

        self.Integrator = self.Integrator + self.error

        if self.Integrator > self.Integrator_max:
            self.Integrator = self.Integrator_max
        elif self.Integrator < self.Integrator_min:
            self.Integrator = self.Integrator_min

        self.I_value = self.Integrator * self.Ki

        pid = self.P_value + self.I_value + self.D_value
        return pid

    def setPoint(self,set_point):
        """
        Initilize the setpoint of PID
        """
        self.set_point = set_point
        self.Integrator=0
        self.Derivator=0

    def setIntegrator(self, Integrator):
        self.Integrator = Integrator

    def setDerivator(self, Derivator):
        self.Derivator = Derivator

    def setKp(self,P):
        self.Kp=P

    def setKi(self,I):
        self.Ki=I

    def setKd(self,D):
        self.Kd=D

    def getPoint(self):
        return self.set_point

    def getError(self):
        return self.error

    def getIntegrator(self):
        return self.Integrator

    def getDerivator(self):
        return self.Derivator

def sign(byte):
    if byte > 127:
        return (256-byte) * (-1)
    else:
        return byte
    
#######	Example	#########


'''
from ToshibaClass import toshibaImage
from ToshibaSerialCCU import ToshibaCCU
import numpy as np
import cv2

cam = toshibaImage(1)
ccu = ToshibaCCU('COM5')

Bset = np.random.randint(201)-100
Rset = np.random.randint(201)-100

ccu.SetWbRgain(Rset)
ccu.SetWbBgain(Bset)

i=1
while True:   
    rgb = np.mean(np.mean(cam.getImage()[:,:,::-1],axis = 0),axis = 0)
    ry = 0.7874*rgb[0]-.7152*rgb[1]-.0722*rgb[2]
    by = -.2126*rgb[0]-.7152*rgb[1]+.9278*rgb[2]
    print(str(i)+' R-Y: '+str(ry)+' B-Y: '+str(by))

    rgain = sign(ccu.GetWbRgain())
    bgain = sign(ccu.GetWbBgain())



    if abs(ry) < 0.5 and abs(by) < 0.5:
        break


    pR=PID(2.0,2.4,1.8)
    pR.setPoint(0)
    pB=PID(2.0,2.4,1.8)
    pB.setPoint(0)

    pidR = pR.update(ry)
    rgain += 0.1*pidR
    rgain = np.int(rgain)
    pidB = pB.update(by)
    bgain += 0.1*pidB
    bgain = np.int(bgain)
    #print ('PidR: '+str(pidR)+' PidB: '+str(pidB)+'\n')

    ccu.SetWbRgain(rgain)
    ccu.SetWbBgain(bgain)
    i+=1
    
print('Manual RGB: '+str(rgb))
print ('RG: '+str(rgain)+' BG: '+str(bgain))

ccu.SetWbMode(0)
rgb = np.mean(np.mean(cam.getImage()[:,:,::-1],axis = 0),axis = 0)
print('Manual RGB: '+str(rgb))
ccu.SetWbMode(2)


cam.close()
ccu.close()
'''