# -*- coding: utf-8 -*-
"""
Created on Thu Aug 25 12:56:12 2016

@author: optics.testone
"""

from cv2 import VideoCapture, imwrite
import numpy as np
import matplotlib.pyplot as plt
from scipy.misc import imread, imsave
import time

class toshibaImage:
    def __init__(self,dev = 0):
        self.cam = VideoCapture(dev)
        
        self.setRes()
        if self.status():        
            print('Successfully initiated Video Capture!')
        else:
            print('ERROR: Video Capture went goofy, or wrong resolution, try again')
    
    def getImage(self):
        # Flush Buffer        
        self.cam.read()
        
        s,self.img = self.cam.read()
                
        if s:
            #220x220 crop
            self.img = self.img[250:470,530:750,:]          
            return self.img
        else:
            print("Image Capture Unsuccessful")
        
    def setRes(self, x = 1280, y = 720):    
        self.cam.set(3,x)
        self.cam.set(4,y)
        
    def avgImage(self,avgs):
        images = []
        st = time.time()        
        for i in range(avgs):
            images.append(self.getImage())
        #print (time.time()-st)
        
        images = np.array(images , dtype = 'uint16')
        images = sum(images)/avgs
        images = np.around(images)
        self.img = np.array(images , dtype = 'uint8')
    
    def getAvgImage(self):
        return self.img
        
    def showImage(self):
        plt.imshow(self.img)
        plt.show()
        
    def saveImage(self,fn):
        imwrite(fn, self.img)
    
    def status(self):
        return self.getImage().shape == (220,220,3)

    def close(self):
        self.cam.release()
    
    def __del__(self):
        self.close()