# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 13:26:57 2016

@author: Yampy
"""

from zaber.serial import AsciiSerial, AsciiDevice

class ZaberStage:
    def __init__(self,com,smax = 9999999,smin = 0):
        self.com = com        
        self.port = AsciiSerial(self.com)
        self.stage = AsciiDevice(self.port , 1)
        reply = self.stage.send("set limit.max "+str(smax))
        reply = self.stage.send("set limit.min "+str(smin))
    
    def __del__(self):
        self.port.close()
    
    def reset(self):   
        reply = self.stage.send("system restore")
        self.stage.send("move abs 0")
        self.stage.poll_until_idle()
        reply = self.getPositionNat()
        
        '''        
        if reply == 0.0:
            print ("Stage at "+self.com+" has been reset!")
        else:
            print ("ERROR while resetting stage at "+self.com)
        '''
        
    def getPosition(self):
        reply = self.stage.send("get pos")
        return float(str(reply).split(' ')[-1])*0.00009921875 #Converts from microsteps into mm
    
    def getPositionNat(self):
        reply = self.stage.send("get pos")
        return float(str(reply).split(' ')[-1])

    def getCurrent(self):
        reply = self.stage.send("get comm.rs232.baud")
        return reply   
    
    def moveAbsolute(self,move):
        move = int(move / 0.00009921875) # for a move in mm, turns into microsteps
        self.stage.send("move abs "+str(move))
        self.stage.poll_until_idle()

    def moveAbsoluteNat(self,move):
        move = int(move)
        self.stage.send("move abs "+str(move))
        self.stage.poll_until_idle()
    
    def close(self):
        self.port.close()
        