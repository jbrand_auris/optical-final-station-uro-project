# -*- coding: utf-8 -*-
"""
Created on Mon Dec  5 16:16:38 2016

@author: steven.yampolsky
"""

'''
Given an image, takes the derivative (finds edges) draws lines on those edges to extract angles and center (by computing where lines intersect)
'''

import cv2
import numpy as np
import matplotlib.pyplot as plt
import sys


def get_cent_angle(img, n_largest=30, search_mod=3, verbose=False):
    if len(img.shape) == 3:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # density = 1.0

    if verbose:
        fig = plt.figure()
        fig.canvas.set_window_title('Original')
        plt.imshow(img)
        plt.show()

    edges = img.copy()
    edges = cv2.Canny(edges, 50, 150, apertureSize=3)

    if verbose:
        fig = plt.figure()
        fig.canvas.set_window_title('Edges')
        plt.imshow(edges)
        plt.show()

    # Search for line using Hough, Find EXACTLY 4 line sets
    j = 70
    num_lines = 4
    while True:

        lines = cv2.HoughLines(edges, 1, np.pi / 360, j, max_theta=np.pi)
        lines = lines.reshape(len(lines), 2)
        lines = lines[lines[:, 1].argsort()]

        angles = np.array(lines).T[1]
        split = []
        for i in range(len(angles) - 1):
            # If change is greater than ~30 deg
            if angles[i + 1] - angles[i] > 0.5:
                split.append(i + 1)

        if len(split) + 1 > num_lines:
            j += 10
        elif j == 0:
            break
        elif len(split) + 1 < num_lines:
            j -= 10
        else:
            break

    # get X,Y for lines found
    lines_pts = []
    for rho, theta in lines:
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a * rho
        y0 = b * rho
        x1 = int(x0 + 1000 * (-b))
        y1 = int(y0 + 1000 * (a))
        x2 = int(x0 - 1000 * (-b))
        y2 = int(y0 - 1000 * (a))
        lines_pts.append([x1, y1, x2, y2])

    # Separate lines by split
    lines_list = []
    for i in range(num_lines):
        if i == 0:
            lines_list.append(lines_pts[0:split[0]])
        elif not i == num_lines - 1:
            lines_list.append(lines_pts[split[i - 1]:split[i]])
        else:
            lines_list.append(lines_pts[split[i - 1]:])

    # Separate angles by split
    angles_list = []
    for i in range(num_lines):
        if i == 0:
            angles_list.append(angles[0:split[0]])
        elif not i == num_lines - 1:
            angles_list.append(angles[split[i - 1]:split[i]])
        else:
            angles_list.append(angles[split[i - 1]:])

    # Consolidate all lines found to one line per split based on how many 255's it runs over
    angle_final = []
    line_final = []
    line_sums = []
    for i in range(num_lines):
        line_profile = []
        for j in lines_list[i]:
            j = np.array(j)
            sums = np.sum(createLineIterator(j[0:2], j[2:], edges))
            line_profile.append(sums)
        line_profile_max = np.argmax(line_profile)
        line_sums.append(line_profile[line_profile_max])
        line_final.append(lines_list[i][line_profile_max])
        angle_final.append(angles_list[i][line_profile_max])

    # Fatten the edge and see how many drops there are, know if youre on sawtooth or smooth edge
    fat_edges = fatten_canny(edges)
    line_final = np.array(line_final)
    jump_sum = []
    for j in line_final:
        line = createLineIterator(j[0:2], j[2:], fat_edges)
        jumps = 0
        for i in range(len(line)-1):
            if np.abs(line[i]-line[i+1]):
                jumps += 1
        jump_sum.append(jumps)

        if verbose:
            fig = plt.figure()
            fig.canvas.set_window_title('Line: ' + str(len(jump_sum)) + ' Jumps: ' + str(jumps))
            plt.plot(line)
            plt.show()

    if np.argmax(jump_sum)%2 == 1:
        angle_final = 180*np.array(angle_final[0::2])/np.pi
    else:
        angle_final = 180*np.array(angle_final[1::2])/np.pi

    if verbose:
        print('Angles: ' + str(angle_final))
        print('Jump Sums' + str(jump_sum))
        print('Lines: ' + str(line_final))

    if verbose:
        for i in line_final:
            cv2.line(img, (i[0], i[1]), (i[2], i[3]), (255, 0, 0), 1)
        fig = plt.figure()
        fig.canvas.set_window_title('Hough')
        plt.imshow(img)
        plt.show()

    # Find Intersections
    sects = []
    for i in lines_list:
        for j in lines_list[lines_list.index(i) + 1:]:
            for k in i:
                for l in j:
                    k = np.array(k)
                    l = np.array(l)
                    sects.append(seg_intersect(k[0:2], k[2:], l[0:2], l[2:]))

    # Calculate Center from Intersects
    center = np.nanmean(sects, axis=0)
    if verbose:
        print('Center at: ' + str(center))

    return [center, angle_final]


def perp(a):
    b = np.empty_like(a)
    b[0] = -a[1]
    b[1] = a[0]
    return b


# line segment a given by endpoints a1, a2
# line segment b given by endpoints b1, b2
# return 
def seg_intersect(a1, a2, b1, b2):
    da = a2 - a1
    db = b2 - b1
    dp = a1 - b1
    dap = perp(da)
    denom = np.dot(dap, db)
    num = np.dot(dap, dp)
    return (num / denom.astype(float)) * db + b1


def createLineIterator(P1, P2, img):
    """
    Produces and array that consists of the coordinates and intensities of each pixel in a line between two points

    Parameters:
        -P1: a numpy array that consists of the coordinate of the first point (x,y)
        -P2: a numpy array that consists of the coordinate of the second point (x,y)
        -img: the image being processed

    Returns:
        -it: a numpy array that consists of the coordinates and intensities of each pixel in the radii (shape: [numPixels, 3], row = [x,y,intensity])     
    """
    # define local variables for readability
    imageH = img.shape[0]
    imageW = img.shape[1]
    P1X = P1[0]
    P1Y = P1[1]
    P2X = P2[0]
    P2Y = P2[1]

    # difference and absolute difference between points
    # used to calculate slope and relative location between points
    dX = P2X - P1X
    dY = P2Y - P1Y
    dXa = np.abs(dX)
    dYa = np.abs(dY)

    # predefine numpy array for output based on distance between points
    itbuffer = np.empty(shape=(np.maximum(dYa, dXa), 3), dtype=np.float32)
    itbuffer.fill(np.nan)

    # Obtain coordinates along the line using a form of Bresenham's algorithm
    negY = P1Y > P2Y
    negX = P1X > P2X
    if P1X == P2X:  # vertical line segment
        itbuffer[:, 0] = P1X
        if negY:
            itbuffer[:, 1] = np.arange(P1Y - 1, P1Y - dYa - 1, -1)
        else:
            itbuffer[:, 1] = np.arange(P1Y + 1, P1Y + dYa + 1)
    elif P1Y == P2Y:  # horizontal line segment
        itbuffer[:, 1] = P1Y
        if negX:
            itbuffer[:, 0] = np.arange(P1X - 1, P1X - dXa - 1, -1)
        else:
            itbuffer[:, 0] = np.arange(P1X + 1, P1X + dXa + 1)
    else:  # diagonal line segment
        steepSlope = dYa > dXa
        if steepSlope:
            slope = dX.astype(np.float32) / dY.astype(np.float32)
            if negY:
                itbuffer[:, 1] = np.arange(P1Y - 1, P1Y - dYa - 1, -1)
            else:
                itbuffer[:, 1] = np.arange(P1Y + 1, P1Y + dYa + 1)
            itbuffer[:, 0] = (slope * (itbuffer[:, 1] - P1Y)).astype(np.int) + P1X
        else:
            slope = dY.astype(np.float32) / dX.astype(np.float32)
            if negX:
                itbuffer[:, 0] = np.arange(P1X - 1, P1X - dXa - 1, -1)
            else:
                itbuffer[:, 0] = np.arange(P1X + 1, P1X + dXa + 1)
            itbuffer[:, 1] = (slope * (itbuffer[:, 0] - P1X)).astype(np.int) + P1Y

            # Remove points outside of image
    colX = itbuffer[:, 0]
    colY = itbuffer[:, 1]
    itbuffer = itbuffer[(colX >= 0) & (colY >= 0) & (colX < imageW) & (colY < imageH)]

    # Get intensities from img ndarray
    itbuffer[:, 2] = img[itbuffer[:, 1].astype(np.uint8), itbuffer[:, 0].astype(np.uint8)]

    return itbuffer[:, 2]
    '''
    def first_n_highest(a, n):
        #returns first n highest members in array, assumes n is smaller than len(a)
        tracker = np.ones(n)*sys.maxint
        for i in a:
            if i > a[0]:
                a[0] = i
    '''


def find_n_largest(a, n):
    # using check_next, returns n largest members in array a
    a_n = np.zeros(n)
    for i in a:
        if i > a_n[0]:
            a_n[0] = i
            a_n = check_next(a_n)
    return np.array(a_n)


def check_next(n, j=0):
    if j == len(n) - 1:
        return n
    elif n[j] > n[j + 1]:
        temp = n[j]
        n[j] = n[j + 1]
        n[j + 1] = temp
        return check_next(n, j + 1)
    else:
        return n

def fatten_canny(img):
    img_sample = img.copy()
    nlin, npix = img.shape
    for i in range(1, nlin - 1):
        for j in range(1, npix - 1):
            if img[i, j] == 255:
                img_sample[i - 1, j - 1] = 255
                img_sample[i - 1, j] = 255
                img_sample[i - 1, j + 1] = 255
                img_sample[i, j - 1] = 255
                img_sample[i, j + 1] = 255
                img_sample[i + 1, j - 1] = 255
                img_sample[i + 1, j] = 255
                img_sample[i + 1, j + 1] = 255
    return img_sample


'''
#Usage:
img_loc = 'C:\\Users\\optics.testone\\Desktop\\Camera Test\\Data\\Toshiba_SN_Repo P008-02\\2016-12-19_11-04-49\\(A)_Alignment_Check\\Check_Position_1.bmp'
img = cv2.imread(img_loc)
derp = get_cent_angle(img,verbose=True)
print (derp)
'''
