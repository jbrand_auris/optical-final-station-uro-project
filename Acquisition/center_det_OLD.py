# -*- coding: utf-8 -*-
"""
Created on Mon Dec  5 16:16:38 2016

@author: steven.yampolsky
"""

'''
Given an image, takes the derivative (finds edges) draws lines on those edges to extract angles and center (by computing where lines intersect)
'''




import cv2
import numpy as np
import matplotlib.pyplot as plt
import sys

def get_cent_angle(img, n_largest = 30, search_mod = 3, verbose = False):
    if len(img.shape) == 3:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        #density = 1.0

    if verbose:
        print('Orig Img')
        plt.imshow(img)
        plt.show()

    '''
    d_img = cv2.Laplacian(img,cv2.CV_64F)
    d_img = np.abs(d_img)
    d_img = np.uint8(d_img)
    
    if verbose:
        print('Lap')
        plt.imshow(d_img)
        plt.show()
    '''
    d_img = img.copy()
    
    edges = cv2.Canny(d_img,50,150,apertureSize = 3)
    
    if verbose:
        print('Edges')
        plt.imshow(edges)
        plt.show()
    
    j = 70
    while True:        
        lines = cv2.HoughLines(edges,1,np.pi/360,j, max_theta = np.pi)
        lines = lines.reshape(len(lines),2)
        lines = lines[lines[:, 1].argsort()]
        
        angles = np.array(lines).T[1]
        split = []
        #print (angles)
        for i in range(len(angles)-1):
            #If change is greater than ~30 deg
            if angles[i+1]-angles[i] > 0.5:
                #print (i)
                split.append(i+1)
        
        #print(j)
        
        if len(split) + 1 > 4:
            j+=10
        elif j == 0:
            break
        elif len(split) + 1 < 4:
            j-=10

        else:
            break

    mod = search_mod
    #print (lines)
    lines_pts = []
    for rho,theta in lines:
        
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*rho
        y0 = b*rho
        x1 = int(x0 + 1000*(-b))
        x10 = int(x0 + mod + 1000*(-b))
        x11 = int(x0 - mod + 1000*(-b))
        y1 = int(y0 + 1000*(a))
        y10 = int(y0 + mod + 1000*(a))
        y11 = int(y0 - mod + 1000*(a))
        x2 = int(x0 - 1000*(-b))
        x20 = int(x0 + mod - 1000*(-b))
        x21 = int(x0 - mod - 1000*(-b))
        y2 = int(y0 - 1000*(a))
        y20 = int(y0 + mod - 1000*(a))
        y21 = int(y0 - mod - 1000*(a))
    
        lines_pts.append([x1,y1,x2,y2])
        lines_pts.append([x10,y10,x20,y20])
        lines_pts.append([x11,y11,x21,y21])
        cv2.line(d_img,(x1,y1),(x2,y2),(255,0,0),2)
        cv2.line(d_img,(x10,y10),(x20,y20),(255,0,0),2)
        cv2.line(d_img,(x11,y11),(x21,y21),(255,0,0),2)
    
    if verbose:
        print(split)
        print(angles)
    

    if verbose:
        print('Hough')
        plt.imshow(d_img)
        plt.show()

    split_list = []
    
    split_list.append(angles[0:split[0]])    
    split_list.append(angles[split[0]:split[1]])
    split_list.append(angles[split[1]:split[2]])
    split_list.append(angles[split[2]:])

    angle_std = [np.std(split_list[0]),np.std(split_list[1]),np.std(split_list[2]),np.std(split_list[3])]
    angle1 = np.argmin(angle_std)
    if angle1 > 1:
        angle1 -=2

    #split_list = split_list[angle1::2]

    sum_angles = []
    for i in split_list:
        sum_angles.append(180*np.mean(np.array(i))/np.pi)

    if verbose:        
        print('Angles: '+str(sum_angles))
    
    one = np.array(lines_pts[0:3*split[0]])
    two = np.array(lines_pts[3*split[0]:3*split[1]])
    three = np.array(lines_pts[3*split[1]:3*split[2]])
    four = np.array(lines_pts[3*split[2]:])
    
       
    
    one_jumps = []
    for i in one:
        one_jumps.append(np.std(np.array(createLineIterator(i[0:2],i[2:],img)-np.roll(np.array(createLineIterator(i[0:2],i[2:],img)),1))[1:-2]))
    one_jumps_n = np.mean(one_jumps)
    
    two_jumps = []
    for i in two:
        two_jumps.append(np.std(np.array(createLineIterator(i[0:2],i[2:],img)-np.roll(np.array(createLineIterator(i[0:2],i[2:],img)),1))[1:-2]))
    two_jumps_n = np.mean(two_jumps)
    

    three_jumps = []
    for i in three:
        three_jumps.append(np.std(np.array(createLineIterator(i[0:2],i[2:],img)-np.roll(np.array(createLineIterator(i[0:2],i[2:],img)),1))[1:-2]))
    three_jumps_n = np.mean(three_jumps)
    
    
    four_jumps = []
    for i in four:
        four_jumps.append(np.std(np.array(createLineIterator(i[0:2],i[2:],img)-np.roll(np.array(createLineIterator(i[0:2],i[2:],img)),1))[1:-2]))
    four_jumps_n = np.mean(four_jumps) 
    
    
    maxhunt = n_largest
    test = []
    for i in range(2,maxhunt):
        one_jumps_test = np.mean(find_n_largest(one_jumps,i))
        two_jumps_test = np.mean(find_n_largest(two_jumps,i))
        three_jumps_test = np.mean(find_n_largest(three_jumps,i))
        four_jumps_test = np.mean(find_n_largest(four_jumps,i))
        test.append(np.array([one_jumps_test,two_jumps_test,three_jumps_test,four_jumps_test]))
    
    jumps = np.array([one_jumps_n,two_jumps_n,three_jumps_n,four_jumps_n])
    jumps_test = np.mean(np.array(test),axis = 0)

    if verbose:
        print ('Mean of Jumps = '+str(np.round(jumps,2)))
        print('Mean of x highest Jumps = '+str(np.round(jumps_test,2)))    
    
    angle1 = np.argmin(jumps)
    if angle1 > 1:
        angle1 -=2
    
    sum_angles = sum_angles[angle1::2]
        
    one_sects = []
    for i in one:
        for j in two:    
            one_sects.append(seg_intersect(i[0:2],i[2:],j[0:2],j[2:]))
        for j in three:    
            one_sects.append(seg_intersect(i[0:2],i[2:],j[0:2],j[2:]))
        for j in four:    
            one_sects.append(seg_intersect(i[0:2],i[2:],j[0:2],j[2:]))
    two_sects = []
    for i in two:
        for j in three:    
            two_sects.append(seg_intersect(i[0:2],i[2:],j[0:2],j[2:]))
        for j in four:    
            two_sects.append(seg_intersect(i[0:2],i[2:],j[0:2],j[2:]))
    three_sects = []
    for i in three:
        for j in four:    
            three_sects.append(seg_intersect(i[0:2],i[2:],j[0:2],j[2:]))
            
    sum_intersect = np.concatenate((one_sects,two_sects,three_sects))
    center = np.nanmean(sum_intersect, axis = 0)

    return [center,sum_angles]

def perp( a ) :
    b = np.empty_like(a)
    b[0] = -a[1]
    b[1] = a[0]
    return b

# line segment a given by endpoints a1, a2
# line segment b given by endpoints b1, b2
# return 
def seg_intersect(a1,a2, b1,b2) :
    da = a2-a1
    db = b2-b1
    dp = a1-b1
    dap = perp(da)
    denom = np.dot( dap, db)
    num = np.dot( dap, dp )
    return (num / denom.astype(float))*db + b1

def createLineIterator(P1, P2, img):
    """
    Produces and array that consists of the coordinates and intensities of each pixel in a line between two points

    Parameters:
        -P1: a numpy array that consists of the coordinate of the first point (x,y)
        -P2: a numpy array that consists of the coordinate of the second point (x,y)
        -img: the image being processed

    Returns:
        -it: a numpy array that consists of the coordinates and intensities of each pixel in the radii (shape: [numPixels, 3], row = [x,y,intensity])     
    """
   #define local variables for readability
    imageH = img.shape[0]
    imageW = img.shape[1]
    P1X = P1[0]
    P1Y = P1[1]
    P2X = P2[0]
    P2Y = P2[1]

    #difference and absolute difference between points
    #used to calculate slope and relative location between points
    dX = P2X - P1X
    dY = P2Y - P1Y
    dXa = np.abs(dX)
    dYa = np.abs(dY)

    #predefine numpy array for output based on distance between points
    itbuffer = np.empty(shape=(np.maximum(dYa,dXa),3),dtype=np.float32)
    itbuffer.fill(np.nan)

    #Obtain coordinates along the line using a form of Bresenham's algorithm
    negY = P1Y > P2Y
    negX = P1X > P2X
    if P1X == P2X: #vertical line segment
        itbuffer[:,0] = P1X
        if negY:
            itbuffer[:,1] = np.arange(P1Y - 1,P1Y - dYa - 1,-1)
        else:
            itbuffer[:,1] = np.arange(P1Y+1,P1Y+dYa+1)              
    elif P1Y == P2Y: #horizontal line segment
        itbuffer[:,1] = P1Y
        if negX:
            itbuffer[:,0] = np.arange(P1X-1,P1X-dXa-1,-1)
        else:
            itbuffer[:,0] = np.arange(P1X+1,P1X+dXa+1)
    else: #diagonal line segment
        steepSlope = dYa > dXa
        if steepSlope:
            slope = dX.astype(np.float32)/dY.astype(np.float32)
            if negY:
                itbuffer[:,1] = np.arange(P1Y-1,P1Y-dYa-1,-1)
            else:
                itbuffer[:,1] = np.arange(P1Y+1,P1Y+dYa+1)
            itbuffer[:,0] = (slope*(itbuffer[:,1]-P1Y)).astype(np.int) + P1X
        else:
            slope = dY.astype(np.float32)/dX.astype(np.float32)
            if negX:
                itbuffer[:,0] = np.arange(P1X-1,P1X-dXa-1,-1)
            else:
                itbuffer[:,0] = np.arange(P1X+1,P1X+dXa+1)
            itbuffer[:,1] = (slope*(itbuffer[:,0]-P1X)).astype(np.int) + P1Y   

    #Remove points outside of image
    colX = itbuffer[:,0]
    colY = itbuffer[:,1]
    itbuffer = itbuffer[(colX >= 0) & (colY >=0) & (colX<imageW) & (colY<imageH)]

    
    #Get intensities from img ndarray
    itbuffer[:,2] = img[itbuffer[:,1].astype(np.uint8),itbuffer[:,0].astype(np.uint8)]

    return itbuffer[:,2]
    '''
    def first_n_highest(a, n):
        #returns first n highest members in array, assumes n is smaller than len(a)
        tracker = np.ones(n)*sys.maxint
        for i in a:
            if i > a[0]:
                a[0] = i
    '''
def find_n_largest(a, n):
    #using check_next, returns n largest members in array a    
    a_n = np.zeros(n)    
    for i in a:
        if i > a_n[0]:
            a_n[0] = i
            a_n = check_next(a_n)
    return np.array(a_n)

def check_next(n,j = 0):
    if j == len(n)-1:
        return n
    elif n[j] > n[j+1]:
        temp = n[j]
        n[j] = n[j+1]
        n[j+1] = temp
        return check_next(n,j+1)
    else:
        return n
'''
#Usage:
img_loc = 'C:\\Users\\optics.testone\\Desktop\\Camera Test\\Data\\Toshiba_SN_Repo P008-02\\2016-12-19_11-04-49\\(A)_Alignment_Check\\Check_Position_1.bmp'
img = cv2.imread(img_loc)
derp = get_cent_angle(img,verbose=True)
print (derp)
'''