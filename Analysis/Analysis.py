from Analysis.MTF_analysis_v3 import MTF
from Analysis.FOV_v4 import fov_v4
from Analysis.pointing_v4 import pointing_v4
from Analysis.Uniformity_v1 import Uniformity_analysis
from Analysis.Dynamic_Range_v2 import dynamicRange
from Analysis.PassFail import passfail


class CameraAnalysis:
    def __init__(self, path, study, datetime, verbose = False):
        self.path = path
        self.study = study
        self.datetime = datetime
        self.verbose = verbose

    def run_MTF(self):
        MTF(self.path, self.study, self.datetime, verbose = self.verbose)
        
    def run_FOV(self):
        fov_v4(self.path, self.study, self.datetime, verbose = self.verbose)
        
    def run_Pointing(self):
        pointing_v4(self.path, self.study, self.datetime, verbose = self.verbose)

    def run_Dynamic_Range(self):
        dynamicRange(self.path, self.study, self.datetime, verbose = self.verbose)
    
    def run_passfail(self):
        passfail(self.path, self.study, self.datetime, verbose = self.verbose)
    
    def run_Uniformity_analysis(self):
        Uniformity_analysis(self.path, self.study, self.datetime, verbose = self.verbose)
        
    def __del__(self):
        pass


# Usage
'''
folder = 'C:\\Users\\steven.yampolsky\\Desktop\\Toshiba_SN_31000001\\2017-06-07_06-38-52'
analysis = CameraAnalysis(folder, 'Toshiba_SN_31000001', '2017-06-07_06-38-52', verbose = False)
analysis.run_MTF()
analysis.run_FOV_Pointing()
analysis.run_Dynamic_Range()
analysis.run_Uniformity_analysis()
analysis.run_passfail()
'''
'''
folder = 'C:\\Users\\steven.yampolsky\\Box Sync\\Camera Production Test\\Camera Calibration\\Data'
Datas = os.listdir(folder)
for i in Datas:
    dates = os.listdir(os.path.join(folder,i))
    for j in dates:
        path = os.path.join(folder,i,j)
        print(path)
        analysis = CameraAnalysis(path, verbose=False)
        analysis.run_MTF()
        analysis.run_FOV_Pointing()
        analysis.run_Dynamic_Range()
        analysis.run_passfail()
'''