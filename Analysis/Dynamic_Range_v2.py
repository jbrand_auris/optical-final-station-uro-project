import numpy as np
import cv2
import matplotlib.pyplot as plt
import os
import pandas as pd
from scipy.ndimage.filters import gaussian_filter
import math


class dynamicRange:
    def __init__(self, path, study, datetime, verbose = False):
        print("\nStarting Dynamic Range Analysis...")
        self.study = study
        self.datetime = datetime
        self.path = path
        if not os.path.exists(os.path.join(self.path, '3_dynamic_range')):
            os.mkdir(os.path.join(self.path, '3_dynamic_range'))
        self.save_path = os.path.join(self.path, '3_dynamic_range')

        self.file = 'd_dynamic_range\\dynamic_range.bmp'
        self.img = cv2.imread(os.path.join(self.path, self.file))
        self.data_path = os.path.join(self.path, self.study+'_'+self.datetime+'_'+'output.csv')

        self.max_dnr = self.getDynamicRange(self.img, verbose)

        self.entry = ['max_dynamic_range_resolved']
        self.data = [self.max_dnr]
        self.dataFrame_add = pd.DataFrame({'value': self.data, 'attribute': self.entry})
        self.dataFrame = pd.read_csv(self.data_path)
        self.data_to_save = pd.concat([self.dataFrame, self.dataFrame_add], ignore_index=True)
        self.data_to_save.to_csv(os.path.join(self.path, self.study+'_'+self.datetime+'_'+'output.csv'), index=False)

    def getDynamicRange(self, img, verbose):

        if len(img.shape) == 3:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        img_markup_1 = img.copy()
        img_markup_2 = img.copy()

        fig = plt.figure()
        fig.canvas.set_window_title('1. Original')
        plt.imshow(img)
        fig.savefig(os.path.join(self.save_path, 'original.png'), bbox_inches='tight')
        if verbose:
            plt.show()
        else:
            plt.close(fig)

        img_markup_1 = self.polarizeImg(img_markup_1)

        fig = plt.figure()
        fig.canvas.set_window_title('2. Polarized')
        plt.imshow(img_markup_1)
        fig.savefig(os.path.join(self.save_path, 'polarized.png'), bbox_inches='tight')
        if verbose:
            plt.show()
        else:
            plt.close(fig)

        img_markup_1 = cv2.Canny(img_markup_1, 50, 150, apertureSize=3)

        fig = plt.figure()
        fig.canvas.set_window_title('3. Canny Polarized')
        plt.imshow(img_markup_1)
        fig.savefig(os.path.join(self.save_path, 'canny_polarized.png'), bbox_inches='tight')
        if verbose:
            plt.show()
        else:
            plt.close(fig)

        x, y, w, h = cv2.boundingRect(img_markup_1)
        cv2.rectangle(img_markup_2, (x, y), (x + w, y + h), (255, 255, 255), 2)

        fig = plt.figure()
        fig.canvas.set_window_title('4. Rectangle Frame')
        plt.imshow(img_markup_2)
        fig.savefig(os.path.join(self.save_path, 'rectangle_frame.png'), bbox_inches='tight')
        if verbose:
            plt.show()
        else:
            plt.close(fig)

        cropped_img = img[y:y + h, x:x + w].copy()

        fig = plt.figure()
        fig.canvas.set_window_title('5. Rectangled Cropped Original')
        plt.imshow(img)
        fig.savefig(os.path.join(self.save_path, 'rectangled_cropped_original.png'), bbox_inches='tight')
        if verbose:
            plt.show()
        else:
            plt.close(fig)

        profile_img = cropped_img.copy()
        image_thresh = cropped_img.copy()
        image_thresh = cv2.Canny(image_thresh, 20, 150, apertureSize=3)

        fig = plt.figure()
        fig.canvas.set_window_title('6. Canny Cropped')
        plt.imshow(image_thresh)
        fig.savefig(os.path.join(self.save_path, 'canny_cropped.png'), bbox_inches='tight')
        if verbose:
            plt.show()
        else:
            plt.close(fig)

        lines = cv2.HoughLines(image_thresh, 1, np.pi / 360, 70, max_theta=np.pi / 2)
        if lines is None:
            return 0.0
        else:
            lines = lines.reshape(len(lines), 2)
            lines = lines[lines[:, 1].argsort()]
            lines = np.array(lines)
            rho, theta = np.mean(lines, axis=0)
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a * rho
            y0 = b * rho
            x10 = np.int0(x0 + 1000 * (-b))
            x11 = np.int0(x0 + 1000 * (-b)) - 5
            x12 = np.int0(x0 + 1000 * (-b)) + 5
            y1 = np.int0(y0 + 1000 * (a))
            x20 = np.int0(x0 - 1000 * (-b))
            x21 = np.int0(x0 - 1000 * (-b)) - 5
            x22 = np.int0(x0 - 1000 * (-b)) + 5
            y2 = np.int0(y0 - 1000 * (a))
            cv2.line(cropped_img, (x10, y1), (x20, y2), (255, 0, 0), 1)
            cv2.line(cropped_img, (x11, y1), (x21, y2), (255, 0, 0), 1)
            cv2.line(cropped_img, (x12, y1), (x22, y2), (255, 0, 0), 1)

            fig = plt.figure()
            fig.canvas.set_window_title('7. Profiles Map')
            plt.imshow(cropped_img)
            fig.savefig(os.path.join(self.save_path, 'profiles_map.png'), bbox_inches='tight')
            if verbose:
                plt.show()
            else:
                plt.close(fig)

            dynamic_range_profile = []
            dynamic_range_profile.append(self.createLineIterator([x10, y1], [x20, y2], profile_img)[::-1])
            dynamic_range_profile.append(self.createLineIterator([x11, y1], [x21, y2], profile_img)[::-1])
            dynamic_range_profile.append(self.createLineIterator([x12, y1], [x22, y2], profile_img)[::-1])
            dynamic_range_profile_MEAN = np.mean(np.array(dynamic_range_profile), axis=0)

            ND = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.8, 1.0, 2.0, 3.0, 4.0]

            # Two FIR filter types
            fil1 = [0.5, -0.5]
            # compute lsf (line spread function) first derivative via FIR (1x3) filter fil
            psf = np.convolve(fil1, 10 * dynamic_range_profile_MEAN)
            psf[0] = psf[1]  # d is longer by 1
            psf[-1] = psf[-2]
            reverseDiv = 3 * gaussian_filter(psf, 2)

            mini = np.argmin(reverseDiv[0:50])
            peaks = 0
            peakFound = False

            peaksX = []
            peaksY = []
            runStarted = False
            # print (reverseDiv)
            for i in range(mini, len(reverseDiv) - 1):

                # If run started
                if reverseDiv[i] > -10:
                    runStarted = True

                # if false start
                if runStarted and reverseDiv[i] < -30 and i < 50:
                    peaksX = []
                    peaksY = []
                    peaks = 0
                # If passed Zero
                if reverseDiv[i] < 20:
                    passedZero = True
                # If Peak Found
                if peakFound == False and reverseDiv[i] > reverseDiv[i + 1] and reverseDiv[
                    i] > 20 and passedZero == True:
                    peaks += 1
                    peaksX.append(i)
                    peaksY.append(reverseDiv[i])
                    peakFound = True
                # If Descending
                if peakFound and reverseDiv[i] < reverseDiv[i + 1]:
                    peakFound = False
                    passedZero = False
                # if done
                if runStarted and reverseDiv[i] < -30 and i > 150:
                    break

            # SHOW PEAKS
            fig = plt.figure()
            fig.canvas.set_window_title('8. Profile, Derivative and Smoothed')
            plt.plot(dynamic_range_profile_MEAN)
            plt.plot(psf)
            plt.plot(reverseDiv)
            plt.scatter(peaksX, peaksY)
            plt.ylim((-40, 255))
            if verbose:
                plt.show()
            else:
                plt.close(fig)

            OD_Cross = []
            peak_loc = []
            for i in range(peaks):
                OD_Cross.append('OD_' + str(ND[i]) + '->' + str(ND[i + 1]))
                peak_loc.append(peaksX[i])
                if verbose:
                    print(i + 1, 'OD_' + str(ND[i]) + '->' + str(ND[i + 1]), peaksX[i])

            dataFrame = pd.DataFrame({'value': peak_loc, 'attribute': OD_Cross})
            dataFrame.to_csv(os.path.join(self.save_path, 'dynamic_range_data.csv'))

            if verbose:
                print('Last OD with counts: ' + str(ND[peaks - 1]))

            print("-(1) Dynamic Range Analysis Complete!")
            
            return ND[peaks - 1]

    def createLineIterator(self, P1, P2, img):
        """
        Produces and array that consists of the coordinates and intensities of each pixel in a line between two points

        Parameters:
            -P1: a numpy array that consists of the coordinate of the first point (x,y)
            -P2: a numpy array that consists of the coordinate of the second point (x,y)
            -img: the image being processed

        Returns:
            -it: a numpy array that consists of the coordinates and intensities of each pixel in the radii (shape: [numPixels, 3], row = [x,y,intensity])
        """
        # define local variables for readability
        imageH = img.shape[0]
        imageW = img.shape[1]
        P1X = P1[0]
        P1Y = P1[1]
        P2X = P2[0]
        P2Y = P2[1]

        # difference and absolute difference between points
        # used to calculate slope and relative location between points
        dX = P2X - P1X
        dY = P2Y - P1Y
        dXa = np.abs(dX)
        dYa = np.abs(dY)

        # predefine numpy array for output based on distance between points
        itbuffer = np.empty(shape=(np.maximum(dYa, dXa), 3), dtype=np.float32)
        itbuffer.fill(np.nan)

        # Obtain coordinates along the line using a form of Bresenham's algorithm
        negY = P1Y > P2Y
        negX = P1X > P2X
        if P1X == P2X:  # vertical line segment
            itbuffer[:, 0] = P1X
            if negY:
                itbuffer[:, 1] = np.arange(P1Y - 1, P1Y - dYa - 1, -1)
            else:
                itbuffer[:, 1] = np.arange(P1Y + 1, P1Y + dYa + 1)
        elif P1Y == P2Y:  # horizontal line segment
            itbuffer[:, 1] = P1Y
            if negX:
                itbuffer[:, 0] = np.arange(P1X - 1, P1X - dXa - 1, -1)
            else:
                itbuffer[:, 0] = np.arange(P1X + 1, P1X + dXa + 1)
        else:  # diagonal line segment
            steepSlope = dYa > dXa
            if steepSlope:
                slope = dX.astype(np.float32) / dY.astype(np.float32)
                if negY:
                    itbuffer[:, 1] = np.arange(P1Y - 1, P1Y - dYa - 1, -1)
                else:
                    itbuffer[:, 1] = np.arange(P1Y + 1, P1Y + dYa + 1)
                itbuffer[:, 0] = (slope * (itbuffer[:, 1] - P1Y)).astype(np.int) + P1X
            else:
                slope = dY.astype(np.float32) / dX.astype(np.float32)
                if negX:
                    itbuffer[:, 0] = np.arange(P1X - 1, P1X - dXa - 1, -1)
                else:
                    itbuffer[:, 0] = np.arange(P1X + 1, P1X + dXa + 1)
                itbuffer[:, 1] = (slope * (itbuffer[:, 0] - P1X)).astype(np.int) + P1Y

                # Remove points outside of image
        colX = itbuffer[:, 0]
        colY = itbuffer[:, 1]
        itbuffer = itbuffer[(colX >= 0) & (colY >= 0) & (colX < imageW) & (colY < imageH)]

        # Get intensities from img ndarray
        itbuffer[:, 2] = img[itbuffer[:, 1].astype(np.uint8), itbuffer[:, 0].astype(np.uint8)]

        return itbuffer[:, 2]

    def polarizeImg(self, img):

        for i in range(len(img)):
            for j in range(len(img[i])):
                if img[i, j] < 100:
                    img[i, j] = 0
                else:
                    img[i, j] = 255
        return img



'''
verbose = True
folder = 'C:\\Users\\steven.yampolsky\\Box Sync\\Camera Production Test\\Camera Calibration\\Data\\Toshiba_SN_161010-08-2\\2016-12-16_18-28-33\\(D)_DNR\\DNR.bmp'
getDynamicRange(cv2.imread(folder))
'''
'''
verbose = False
folder = 'C:\\Users\\steven.yampolsky\\Box Sync\\Camera Production Test\\Camera Calibration\\Data'
Datas = os.listdir(folder)
for i in Datas:
    dates = os.listdir(os.path.join(folder,i))
    for j in dates:
        print(dynamicRange(os.path.join(folder,i,j)))
'''
