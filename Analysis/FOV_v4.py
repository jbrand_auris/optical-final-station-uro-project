# -*- coding: utf-8 -*-
"""
Created on Fri Oct 13 13:18:35 2017

@author: Jonathan.Brand
"""

import numpy as np
import cv2
import matplotlib.pyplot as plt
import scipy.optimize as optimization
import os
import pandas as pd

class fov_v4:
    def __init__(self, path, study, datetime, verbose = False):
        
        print("Starting FOV analysis...")

        self.study = study
        self.datetime = datetime
        self.path = path
        
        self.data_path = os.path.join(self.path, self.study+'_'+self.datetime+'_'+'output.csv')
        Z = [23.8125]
        
        if not os.path.exists(os.path.join(self.path, '2_fov_and_pointing')):
            os.mkdir(os.path.join(self.path, '2_fov_and_pointing'))
        if not os.path.exists(os.path.join(self.path, '2_fov_and_pointing','fov')):
            os.mkdir(os.path.join(self.path, '2_fov_and_pointing','fov'))
        self.save_path = os.path.join(self.path, '2_fov_and_pointing','fov')
        
        
        
        self.file = 'k_field_of_view_image\\ring_target_2.bmp'
        self.img = cv2.imread(os.path.join(self.path, self.file),0)
        self.fov, self.fov_raw = self.estimate_fov(self.img, Z[0], verbose)
        if verbose:
            print('FOV = ' + str(np.round(self.fov_1,2)) +' degrees')
        

        entry = [ 'fov_estimate', 'fov_raw']
        data = [self.fov,self.fov_raw]
        dataFrame = pd.DataFrame({'value': data, 'attribute': entry})
        dataFrame.to_csv(os.path.join(self.save_path, 'fov_pointing_data.csv'), index = False)
        
        self.entry = ['field_of_view_degrees']
        self.data = [np.round(self.fov,2)]
        self.dataFrame_add = pd.DataFrame({'value': self.data, 'attribute': self.entry})
        self.dataFrame = pd.read_csv(self.data_path)
        self.data_to_save = pd.concat([self.dataFrame, self.dataFrame_add], ignore_index=True)
        self.data_to_save.to_csv(os.path.join(self.path, self.study+'_'+self.datetime+'_'+'output.csv'), index=False)
        
    def estimate_fov(self, img, Z, verbose=False):
        img = cv2.medianBlur(img,5)
        edges = cv2.Canny(img,175,200)
        #save original
        fig = plt.figure()
        fig.canvas.set_window_title('ring_target_original')
        plt.imshow(img)
        fig.savefig(os.path.join(self.save_path, 'ring_target_original.png'), bbox_inches='tight')
        if verbose:
            plt.show()
        else:
            plt.close(fig)
        #save edge image
        fig = plt.figure()
        fig.canvas.set_window_title('ring_target_edges')
        plt.imshow(edges)
        fig.savefig(os.path.join(self.save_path, 'ring_target_edges.png'), bbox_inches='tight')
        if verbose:
            plt.show()
        else:
            plt.close(fig)
            
        #sample the four arms of the image
        Q2Q4 = np.diagonal(edges) 
        Q1Q3 = np.diagonal(np.rot90(edges))
        if verbose:
            plt.plot(Q2Q4, 'b-')
            plt.show()
            plt.plot(Q1Q3, 'b-')
            plt.show()
        
        if np.min(np.shape(edges)) % 2 == 0:
            center_to_corner = int(np.max(np.shape(edges))/2)
            quadrant_data = np.zeros([4,center_to_corner-1])
        else:
            center_to_corner = int((np.max(np.shape(edges)-1)/2)+1)
            quadrant_data = np.zeros([4,center_to_corner-1])
            
        #populate the matrix with the quadrant image data 
        quadrant_data[0,:] = np.flip(Q1Q3[0:center_to_corner-1],0)
        quadrant_data[1,:] = np.flip(Q2Q4[0:center_to_corner-1],0)
        quadrant_data[2,:] = Q1Q3[center_to_corner:np.max(np.shape(edges))-1]
        quadrant_data[3,:] = Q2Q4[center_to_corner:np.max(np.shape(edges))-1]
        
        if verbose:
            plt.plot(quadrant_data[0,:], 'b-')
            plt.show()
            plt.plot(quadrant_data[1,:], 'b-')
            plt.show()
            plt.plot(quadrant_data[2,:], 'b-')
            plt.show()
            plt.plot(quadrant_data[3,:], 'b-')
            plt.show()
        
        #establish counter
                #radii of rings in mm
        radii = [11.6586, 14.4272, 17.5006, 20.9804, 24.9936, 29.7942, 35.70605]

        hfov = np.zeros([4])
        for idq,m in enumerate(quadrant_data):
            edges  = self.elimintate_dbl_edg(m.nonzero()[0])
            hfov[idq] = 180 * np.arctan(self.fit_data(edges,radii,idq) / Z) / np.pi
            if verbose:
                print('Quadrant'+ str(idq+1) +' HFOV = '+str(hfov[idq]))
        #Find pixels associated with edges 

        FOV_estimate_raw = np.max([hfov[0]+hfov[2],hfov[1]+hfov[3]])
        #Multiply esitmate by estimated scaling factor from Gage R&R data
        FOV_estimate = FOV_estimate_raw*1.045058733
        
        if verbose:
            print('FOV based on average= ' + str(FOV_estimate))
            
        return FOV_estimate, FOV_estimate_raw
        
    def elimintate_dbl_edg(self,edges):
        n=0
        while True:
            try :
                if (edges[n+1]-edges[n])<5:
                    edges = np.delete(edges,n+1)
                else:
                    n = n+1
            except:
                break  
        return edges 
    
    def fit_data(self, edges,radii, idq, verbose = False):
        rad = np.sqrt(2*np.power(edges,2))
        #fit_func = np.poly1d(np.polyfit(np.append(rad,0), np.append(radii[:len(rad)],0), 4))
        def func_quad(x,a,b,c,d,e):
                return  a*x*x*x*x+ b*x*x*x +c*x*x +d*x+e
        
        while True:
            try:
                potp, covp = optimization.curve_fit(func_quad, np.append(rad,0), np.append(radii[:len(rad)],0), bounds=(0, np.max(rad)))
                fit_func = np.poly1d(potp)
                break
            except:
                rad = rad[1:]
        
        fit_x = np.arange(0, 160, 0.5)
        fit_y = fit_func(fit_x)
        fig = plt.figure()
        fig.canvas.set_window_title('distortion ' )
        
        plt.scatter(np.append(rad,0), np.append(radii[:len(rad)],0))
        plt.plot(fit_x, fit_y, 'r-')
        plt.xlim(-10, 160)
        plt.ylim(-5, 40)
        fig.savefig(os.path.join(self.save_path, 'Q'+str(idq+1)+'_edges_and_fit.png'), bbox_inches='tight')
        if verbose:
            plt.show()
        else:
            plt.close(fig)
        
        corner = fit_func(np.sqrt(2*np.power(110,2)))
        return corner
        
        
    def __del__(self):
        pass