# -*- coding: utf-8 -*-
"""
Created on Fri Nov  4 10:41:50 2016
@author: Yampy
"""

import os
import shutil
import cv2
import csv
import numpy as np
import progressbar
import warnings
import pandas as pd
import matplotlib.pyplot as plt
import scipy.interpolate as si

from Analysis.MTF_sfrpy_sy_v3 import sfr, getEdge, contrast_check

warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)


class MTF:
    def __init__(self, path, study, datetime, verbose=False, pix_pitch = 1.75):
        print("\nStarting MTF Analysis...\n")

        self.folder = path
        self.study = study
        self.datetime = datetime
        self.verbose = verbose
        self.pix_pitch = pix_pitch  # um

        self.createDirectories()
        self.rotateOriginals()
        self.optimizeEdgeCropping()
        self.getAndPlotMTF50()

    def createDirectories(self):
        self.MTF_data_loc = os.path.join(self.folder, 'c_mtf')
        if not os.path.exists(os.path.join(self.folder, '1_mtf')):
            os.mkdir(os.path.join(self.folder, '1_mtf'))
        self.MTF_analysis_loc = os.path.join(self.folder, '1_mtf')
        if not os.path.exists(os.path.join(self.folder, self.MTF_analysis_loc, 'original')):
            MTF_files = os.listdir(self.MTF_data_loc)  # List all MTF Files
            os.mkdir(os.path.join(self.folder, self.MTF_analysis_loc, 'original'))
            for i in MTF_files:
                if i[-4:] == '.bmp':
                    shutil.copyfile(os.path.join(self.MTF_data_loc, i),
                                    os.path.join(self.MTF_analysis_loc, 'original', i))

        if not os.path.exists(os.path.join(self.MTF_analysis_loc, 'cropped')):
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'cropped'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'cropped', 'upper'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'cropped', 'lower'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'cropped', 'right'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'cropped', 'left'))

        if not os.path.exists(os.path.join(self.MTF_analysis_loc, 'rotated')):
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'rotated'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'rotated', 'upper'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'rotated', 'lower'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'rotated', 'right'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'rotated', 'left'))

        if not os.path.exists(os.path.join(self.MTF_analysis_loc, 'esf_csv')):
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_csv'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_csv', 'upper'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_csv', 'lower'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_csv', 'right'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_csv', 'left'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_csv', 'upper', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_csv', 'upper', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_csv', 'lower', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_csv', 'lower', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_csv', 'right', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_csv', 'right', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_csv', 'left', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_csv', 'left', 'outer'))

        if not os.path.exists(os.path.join(self.MTF_analysis_loc, 'psf_csv')):
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_csv'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_csv', 'upper'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_csv', 'lower'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_csv', 'right'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_csv', 'left'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_csv', 'upper', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_csv', 'upper', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_csv', 'lower', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_csv', 'lower', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_csv', 'right', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_csv', 'right', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_csv', 'left', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_csv', 'left', 'outer'))

        if not os.path.exists(os.path.join(self.MTF_analysis_loc, 'mtf_csv')):
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_csv'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_csv', 'upper'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_csv', 'lower'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_csv', 'right'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_csv', 'left'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_csv', 'upper', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_csv', 'upper', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_csv', 'lower', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_csv', 'lower', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_csv', 'right', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_csv', 'right', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_csv', 'left', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_csv', 'left', 'outer'))

        if not os.path.exists(os.path.join(self.MTF_analysis_loc, 'esf_plots')):
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_plots'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_plots', 'upper'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_plots', 'lower'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_plots', 'right'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_plots', 'left'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_plots', 'upper', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_plots', 'upper', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_plots', 'lower', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_plots', 'lower', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_plots', 'right', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_plots', 'right', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_plots', 'left', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'esf_plots', 'left', 'outer'))

        if not os.path.exists(os.path.join(self.MTF_analysis_loc, 'psf_plots')):
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_plots'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_plots', 'upper'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_plots', 'lower'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_plots', 'right'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_plots', 'left'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_plots', 'upper', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_plots', 'upper', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_plots', 'lower', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_plots', 'lower', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_plots', 'right', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_plots', 'right', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_plots', 'left', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'psf_plots', 'left', 'outer'))

        if not os.path.exists(os.path.join(self.MTF_analysis_loc, 'mtf_plots')):
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_plots'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_plots', 'upper'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_plots', 'lower'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_plots', 'right'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_plots', 'left'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_plots', 'upper', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_plots', 'upper', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_plots', 'lower', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_plots', 'lower', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_plots', 'right', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_plots', 'right', 'outer'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_plots', 'left', 'inner'))
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf_plots', 'left', 'outer'))

        if not os.path.exists(os.path.join(self.MTF_analysis_loc, 'mtf50_csv')):
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf50_csv'))

        if not os.path.exists(os.path.join(self.MTF_analysis_loc, 'mtf50_plots')):
            os.mkdir(os.path.join(self.MTF_analysis_loc, 'mtf50_plots'))

    def rotateOriginals(self):
        # Index Locations of Original Files
        MTF_files = os.listdir(os.path.join(self.MTF_analysis_loc, 'original'))

        # Rotate and Deposit into appropriate folders
        for i in MTF_files:
            img = cv2.imread(os.path.join(self.MTF_analysis_loc, 'original', i))
            # Upper
            imgT = img
            cv2.imwrite(os.path.join(self.MTF_analysis_loc, 'rotated', 'upper', i), imgT)
            # Lower
            imgB = img[::-1][:, ::-1]
            cv2.imwrite(os.path.join(self.MTF_analysis_loc, 'rotated', 'lower', i), imgB)
            # Left
            imgL = np.transpose(img, [1, 0, 2])[:, ::-1]
            cv2.imwrite(os.path.join(self.MTF_analysis_loc, 'rotated', 'left', i), imgL)
            # Right
            imgR = np.transpose(img, [1, 0, 2])[::-1]
            cv2.imwrite(os.path.join(self.MTF_analysis_loc, 'rotated', 'right', i), imgR)

    def optimizeEdgeCropping(self):
        # start at 1.0mm
        self.start = 2
        self.mtf_angle = []

        # Find Where Edges Start
        for j in ['upper', 'lower', 'left', 'right']:
            MTF_rotated_files_loc = os.path.join(self.MTF_analysis_loc, 'rotated', j)
            MTF_cropped_files_loc = os.path.join(self.MTF_analysis_loc, 'cropped', j)
            MTF_rotated_files = os.listdir(MTF_rotated_files_loc)[self.start:]

            edgeStartList = []

            # Run over all Images where d > 5mm and find where edge begins for respective rotation
            for k in MTF_rotated_files[5:15]:
                img = cv2.imread(os.path.join(MTF_rotated_files_loc, k))
                temp = getEdge(img)
                edgeStartList.append(temp)

            edgeStartList = np.array(edgeStartList)
            temp = np.mean(edgeStartList, axis=0).astype(np.int)
            self.mtf_angle.append(np.mean(edgeStartList, axis=0)[4])
            self.max_pbar = 8 * len(MTF_rotated_files)

            # Use Edge info to crop and place ALL images (except d = 0 mm)
            for k in MTF_rotated_files:
                img = cv2.imread(os.path.join(MTF_rotated_files_loc, k))
                img = img[0:temp[3], temp[1] - temp[2]:temp[0] + temp[2]]
                cv2.imwrite(os.path.join(MTF_cropped_files_loc, k), img)

    def getAndPlotMTF50(self):

        # data prepend for SFR
        x50_data = []
        y5_data = []
        failed = []
        angles = []

        # Carry out MTF

        bar = progressbar.ProgressBar(max_value=self.max_pbar)
        bar_counter = 0

        for j in ['upper', 'lower', 'left', 'right']:
            MTF_cropped_files_loc = os.path.join(self.MTF_analysis_loc, 'cropped', j)
            MTF_cropped_files = os.listdir(MTF_cropped_files_loc)
            angle_iterator = 0
            for l in ['inner', 'outer']:

                MTF_mtf_output_loc = os.path.join(self.MTF_analysis_loc, 'mtf_csv', j, l)
                MTF_esf_output_loc = os.path.join(self.MTF_analysis_loc, 'esf_csv', j, l)
                MTF_psf_output_loc = os.path.join(self.MTF_analysis_loc, 'psf_csv', j, l)
                MTF_mtf_plot_loc = os.path.join(self.MTF_analysis_loc, 'mtf_plots', j, l)
                MTF_esf_plot_loc = os.path.join(self.MTF_analysis_loc, 'esf_plots', j, l)
                MTF_psf_plot_loc = os.path.join(self.MTF_analysis_loc, 'psf_plots', j, l)

                for k in MTF_cropped_files:

                    # Call Out the SFR
                    if self.verbose:
                        print('\n' + str(j) + ' ' + str(l) + ' ' + str(k))

                    # Read Image get number of lines
                    img = cv2.imread(os.path.join(MTF_cropped_files_loc, k))
                    num_lines, _, _ = img.shape
                    splitter = np.int(num_lines / 3)

                    # Select the region
                    if l == 'inner':
                        img = img[2 * splitter - 4:2 * splitter + 4]
                    else:
                        img = img[splitter - 4:splitter + 4]

                    # Contrast Check
                    status = contrast_check(img)

                    if status:
                        # MTF, give it the image, the angle (from edge search), and pixel pitch
                        status, freq, mtf, esf_x, esf_y, esf_fit_x, esf_fit_y, psf = sfr(img,
                                                                                         self.mtf_angle[angle_iterator],
                                                                                         self.pix_pitch, self.verbose)
                        # make a list of failures (fitting sigmoid)
                        if status == 1:
                            failed.append([j, l, k[:-4], 'sigmoid_fit_failed'])

                        if self.verbose:
                            print("lp/mm range: " + str(np.round(freq[-1], 2)))

                            # Save csv of OUTER_ESF
                        csv_file = os.path.join(MTF_esf_output_loc, k[:-4] + '.csv')
                        with open(csv_file, 'w', newline='') as csvfile:
                            csv_writer = csv.writer(csvfile, delimiter=',',
                                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
                            csv_writer.writerow(['pixel', 'esf', 'esf_fit_x', 'esf_fit_y'])
                            for i in range(len(esf_x)):
                                line = []
                                line.append(esf_x[i])
                                line.append(esf_y[i])
                                line.append(esf_fit_x[i])
                                line.append(esf_fit_y[i])
                                csv_writer.writerow(line)

                        # Plot OUTER_ESF
                        self.plot_esf(os.path.join(MTF_esf_output_loc, k[:-4] + '.csv'),
                                      os.path.join(MTF_esf_plot_loc, k[:-4] + '.png'))

                        # Save csv of OUTER_PSF
                        csv_file = os.path.join(MTF_psf_output_loc, k[:-4] + '.csv')
                        with open(csv_file, 'w', newline='') as csvfile:
                            csv_writer = csv.writer(csvfile, delimiter=',',
                                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
                            csv_writer.writerow(['pixel', 'psf'])
                            for i in range(len(esf_fit_x)):
                                line = []
                                line.append(esf_fit_x[i])
                                line.append(psf[i])
                                csv_writer.writerow(line)

                        # Plot OUTER_PSF
                        self.plot_psf(os.path.join(MTF_psf_output_loc, k[:-4] + '.csv'),
                                      os.path.join(MTF_psf_plot_loc, k[:-4] + '.png'))

                        # Save csv of OUTER_MTF
                        csv_file = os.path.join(MTF_mtf_output_loc, k[:-4] + '.csv')
                        with open(csv_file, 'w', newline='') as csvfile:
                            csv_writer = csv.writer(csvfile, delimiter=',',
                                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
                            csv_writer.writerow(['freq_py', 'mtf_py'])
                            for i in range(len(freq)):
                                line = []
                                line.append(freq[i])
                                line.append(mtf[i])
                                csv_writer.writerow(line)
                        # Plot MTF
                        x50, y_5 = self.plot_mtf(os.path.join(MTF_mtf_output_loc, k[:-4] + '.csv'),
                                                 os.path.join(MTF_mtf_plot_loc, k[:-4] + '.png'), self.verbose)

                        # Save SFR50 vs distance
                        x50_data.append([float(k[:4]), x50])
                        y5_data.append([float(k[:4]), y_5])

                    else:
                        # make a list of failures (Contrast Check)
                        if status == 0:
                            failed.append([j, l, k[:-4], 'contrast_check_failed'])
                        x50_data.append([float(k[:4]), np.nan])
                        y5_data.append([float(k[:4]), np.nan])

                    # iterate bar
                    bar_counter += 1
                    bar.update(bar_counter)

                # Note Angle
                angle = self.mtf_angle[angle_iterator]
                angles.append([j, np.round(180 * angle / np.pi, 2)])
                angle_iterator += 1

        # Write Failures
        fail_file = os.path.join(self.MTF_analysis_loc, 'failed.csv')
        with open(fail_file, 'w', newline='') as failfile:
            fail_writer = csv.writer(failfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            fail_writer.writerow(['region', 'region', 'distance', 'angle'])
            for i in failed:
                fail_writer.writerow(i)

        # Write Angles
        angle_file = os.path.join(self.MTF_analysis_loc, 'angles.csv')
        with open(angle_file, 'w', newline='') as anglefile:
            angle_writer = csv.writer(anglefile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            angle_writer.writerow(['region', 'region', 'angle'])
            for i in angles:
                angle_writer.writerow(i)

        # Rearrange MTF50 Data
        data = np.reshape(np.array(x50_data), (8, np.int(len(x50_data) / 8), 2))
        data_5 = np.reshape(np.array(y5_data), (8, np.int(len(y5_data) / 8), 2))
        MTF_output_loc = os.path.join(self.MTF_analysis_loc, 'mtf50_csv')
        MTF_fig_loc = os.path.join(self.MTF_analysis_loc, 'mtf50_plots')

        # MTF-50 Data
        upper_inner = np.array(data[0])
        upper_outer = np.array(data[1])
        lower_inner = np.array(data[2])
        lower_outer = np.array(data[3])
        left_inner = np.array(data[4])
        left_outer = np.array(data[5])
        right_inner = np.array(data[6])
        right_outer = np.array(data[7])

        # Best Focus MTF-50
        BF = []

        # MTF % at 150 lp/mm
        MTF150 = []

        # Write MTF-50 Data and Plot
        counter = 0
        for j in ['upper', 'lower', 'left', 'right']:

            csv_out_inner = os.path.join(MTF_output_loc, j + '_inner.csv')
            csv_out_outer = os.path.join(MTF_output_loc, j + '_outer.csv')

            csv_out_inner_150 = os.path.join(MTF_output_loc, j + '_inner_150.csv')
            csv_out_outer_150 = os.path.join(MTF_output_loc, j + '_outer_150.csv')

            with open(csv_out_inner, "w", newline='') as output_csv:
                writer = csv.writer(output_csv)
                writer.writerow(["distance_mm", "x_50"])
                for i in np.array(data[2 * counter]):
                    writer.writerow([i[0], i[1]])
            with open(csv_out_outer, "w", newline='') as output_csv:
                writer = csv.writer(output_csv)
                writer.writerow(["distance_mm", "x_50"])
                for i in np.array(data[2 * counter + 1]):
                    writer.writerow([i[0], i[1]])

            with open(csv_out_inner_150, "w", newline='') as output_csv:
                writer = csv.writer(output_csv)
                writer.writerow(["distance_mm", "mtf_150_lp-mm"])
                for i in np.array(data_5[2 * counter]):
                    writer.writerow([i[0], i[1]])
            with open(csv_out_outer_150, "w", newline='') as output_csv:
                writer = csv.writer(output_csv)
                writer.writerow(["distance_mm", "mtf_150_lp-mm"])
                for i in np.array(data_5[2 * counter + 1]):
                    writer.writerow([i[0], i[1]])

            inner = self.plot_mtf50(csv_out_inner, os.path.join(MTF_fig_loc, j + '_inner.png'), j + ' Inner')
            outer = self.plot_mtf50(csv_out_outer, os.path.join(MTF_fig_loc, j + '_outer.png'), j + ' Outer')

            inner_150 = self.plot_mtf150(csv_out_inner_150, os.path.join(MTF_fig_loc, j + '_inner_150.png'), j + ' Inner')
            outer_150 = self.plot_mtf150(csv_out_outer_150, os.path.join(MTF_fig_loc, j + '_outer_150.png'), j + ' Outer')

            for i in range(len(inner[0])):
                BF.append([j + '_inner', inner[0][i], np.round(inner[1][i], 2)])
                BF.append([j + '_outer', outer[0][i], np.round(outer[1][i], 2)])
                MTF150.append([j + '_inner', inner_150[0][i], np.round(inner_150[1][i], 2)])
                MTF150.append([j + '_outer', outer_150[0][i], np.round(outer_150[1][i], 2)])
            counter += 1

        # Write MTF-50 at Best Focus to MTF
        bf_file = os.path.join(self.MTF_analysis_loc, 'bf.csv')
        with open(bf_file, 'w', newline='') as bffile:
            bf_writer = csv.writer(bffile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            bf_writer.writerow(['region', 'x', 'y'])
            for i in BF:
                bf_writer.writerow(i)

        # Write MTF-50 at Best Focus to MTF
        mtf5_file = os.path.join(self.MTF_analysis_loc, 'bf.csv')
        with open(mtf5_file, 'w', newline='') as mtf5file:
            mtf5_writer = csv.writer(mtf5file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            mtf5_writer.writerow(['region', 'x', 'y'])
            for i in MTF150:
                mtf5_writer.writerow(i)

        distances = []
        for i in BF:
            distances.append(i[1])
        distances = list(set(distances))

        MTF50s = []
        for i in distances:
            MTF50at = []
            for j in BF:
                if j[1] == i:
                    MTF50at.append(j[2])
            MTF50s.append(MTF50at)
        MTF50s = np.array(MTF50s)
        MTF50s = np.min(MTF50s, axis=1)

        MTF150s = []
        for i in distances:
            MTF150at = []
            for j in MTF150:
                if j[1] == i:
                    MTF150at.append(j[2])
            MTF150s.append(MTF150at)
        MTF150s = np.array(MTF150s)
        MTF150s = np.min(MTF150s, axis=1)

        # Write Avg MTF-50 data to Camera Data



        data_path = os.path.join(self.folder, self.study+'_'+self.datetime+'_'+'output.csv')
        entry = ['mtf-50_' + str(distances[0]) + '_mm',
                 'mtf-50_' + str(distances[1]) + '_mm',
                 'mtf-50_' + str(distances[2]) + '_mm',
                 'mtf-50_' + str(distances[3]) + '_mm',
                 'mtf_150_lp-mm_' + str(distances[0]) + '_mm',
                 'mtf_150_lp-mm_' + str(distances[1]) + '_mm',
                 'mtf_150_lp-mm_' + str(distances[2]) + '_mm',
                 'mtf_150_lp-mm_' + str(distances[3]) + '_mm']
        data = [np.round(MTF50s[0], 2), np.round(MTF50s[1], 2), np.round(MTF50s[2], 2), np.round(MTF50s[3], 2),
                np.round(MTF150s[0], 2), np.round(MTF150s[1], 2), np.round(MTF150s[2], 2), np.round(MTF150s[3], 2)]

        dataFrame_add = pd.DataFrame({'value': data, 'attribute': entry})
        dataFrame = pd.read_csv(data_path)
        data_to_save = pd.concat([dataFrame, dataFrame_add], ignore_index=True)
        data_to_save.to_csv(os.path.join(self.folder, self.study+'_'+self.datetime+'_'+'output.csv'), index=False)

        print("\n-(1) MTF Analysis Completed!")

    def plot_mtf(self, input_csv, output_png, verbose=False):
        data = np.genfromtxt(input_csv,
                             delimiter=',',
                             names=['freq', 'sfr'], skip_header=1)

        data = data[:int(len(data) / 2)]
        x_i, y_i = self.spline(data['freq'], data['sfr'])

        # Search for MTF-50
        i = 0
        search = 50
        while True:
            if i == len(y_i):
                return np.nan
            elif y_i[i] < search:
                if verbose:
                    print('MTF-50: ' + str(np.round(x_i[i], 2)) + ' lp/mm')
                break
            else:
                i += 1
        loc = i

        # MTF % at 150 lp / mm
        i = 0
        search = 150  # lp/mm
        while True:
            if i == len(y_i):
                return np.nan
            elif x_i[i] > search:
                if verbose:
                    print('MTF at 5% : ' + str(np.round(x_i[i], 2)) + ' %')
                break
            else:
                i += 1
        y_loc = i

        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax1.set_title(os.path.basename(input_csv))
        ax1.set_xlabel('SFR (lp/mm)')
        ax1.set_ylabel('MTF')

        ax1.plot(data['freq'], data['sfr'], 'ro', c='r', markersize=6, label='MTF Data')
        ax1.plot(x_i, y_i, '-', c='b', label='Spline')
        ax1.plot([x_i[loc]], [y_i[loc]], '.', c='y', markersize=10, label='sfr 50%')
        ax1.plot([x_i[y_loc]], [y_i[y_loc]], '.', c='g', markersize=10, label='sfr at 150 lp/mm')
        ax1.annotate('x=%.2f' % x_i[loc], xy=(x_i[loc], y_i[loc]), xytext=(250, 70),
                     arrowprops=dict(facecolor='gray', shrink=0.05))
        ax1.annotate('y=%.2f' % np.round(y_i[y_loc], 2), xy=(x_i[y_loc], y_i[y_loc]), xytext=(250, 50),
                     arrowprops=dict(facecolor='gray', shrink=0.05))
        leg = ax1.legend()
        plt.ylim(0, 110)
        fig.savefig(output_png)
        plt.close(fig)

        return x_i[loc], y_i[y_loc]

    def plot_esf(self, input_csv, output_png):
        data = np.genfromtxt(input_csv,
                             delimiter=',',
                             names=['pixel', 'esf', 'esf_fit_x', 'esf_fit_y'], skip_header=1)

        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax1.set_title(os.path.basename(input_csv))
        ax1.set_xlabel('Pixel')
        ax1.set_ylabel('ESF')
        ax1.plot(data['pixel'], data['esf'], 'ro', c='r', label='ESF')
        ax1.plot(data['esf_fit_x'], data['esf_fit_y'], label='ESF Fit')
        plt.ylim(0, 255)
        fig.savefig(output_png)
        plt.close(fig)

    def plot_psf(self, input_csv, output_png):
        data = np.genfromtxt(input_csv,
                             delimiter=',',
                             names=['pixel', 'psf'], skip_header=1)

        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax1.set_title(os.path.basename(input_csv))
        ax1.set_xlabel('Pixel')
        ax1.set_ylabel('PSF')
        ax1.plot(data['pixel'], data['psf'], 'ro', c='r', label='PSF')
        fig.savefig(output_png)
        plt.close(fig)

    def spline(self, x, y):
        t = range(len(x))
        # knots = [2, 3, 4]
        ipl_t = np.linspace(0.0, len(x) - 1, 1000)

        x_tup = si.splrep(t, x, k=3, per=1)
        y_tup = si.splrep(t, y, k=3, per=1)

        x_list = list(x_tup)
        xl = x.tolist()
        x_list[1] = [0.0] + xl + [0.0, 0.0, 0.0, 0.0]

        y_list = list(y_tup)
        yl = y.tolist()
        y_list[1] = [0.0] + yl + [0.0, 0.0, 0.0, 0.0]

        x_i = si.splev(ipl_t, x_list)
        y_i = si.splev(ipl_t, y_list)

        return [x_i, y_i]

    def plot_mtf50(self, input_csv, output_path, name=''):
        data = np.genfromtxt(input_csv,
                             delimiter=',',
                             names=['distance', 'x_50'], skip_header=1)

        x = data['distance']
        y = data['x_50']

        y_nan = np.isnan(y)

        x = x[np.array(np.abs(y_nan - 1), dtype=bool)]
        y = y[np.array(np.abs(y_nan - 1), dtype=bool)]

        # fit_func = np.poly1d(np.polyfit(x, y, 13))
        fit_x = np.arange(0, 50.5, 0.5)
        # fit_y = fit_func(fit_x)
        # fit_y = np.interp(x_sig,x_array,y_array)
        tck = si.splrep(x, y)
        fit_y = si.splev(fit_x, tck)

        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax1.set_title("MTF-50 vs Distance for " + str(name))
        ax1.set_xlabel('Distance (mm)')
        ax1.set_ylabel('MTF-50 (lp/mm)')

        ax1.plot(x, y, 'ro', c='r', label='Data')
        ax1.plot(fit_x, fit_y, 'b-', c='b', label='Fit')

        x_fit = []
        y_fit = []

        counter = 0
        for i in [3, 7, 25, 50]:
            pos_x = self.findClosest(fit_x, i)
            x_fit.append(fit_x[pos_x])
            y_fit.append(fit_y[pos_x])
            ax1.annotate('x=%.2f, y=%.2f' % (fit_x[pos_x], fit_y[pos_x]), xy=(fit_x[pos_x], fit_y[pos_x]),
                         xytext=(3 + 10 * counter, 20 + 15 * counter),
                         arrowprops=dict(facecolor='gray', shrink=0.05))
            counter += 1

        plt.ylim(0, 200)
        ax1.legend(loc=4)

        fig.savefig(output_path)
        plt.close(fig)

        return x_fit, y_fit
        
    def plot_mtf150(self, input_csv, output_path, name=''):
        data = np.genfromtxt(input_csv,
                             delimiter=',',
                             names=['distance', 'x_50'], skip_header=1)

        x = data['distance']
        y = data['x_50']

        y_nan = np.isnan(y)

        x = x[np.array(np.abs(y_nan - 1), dtype=bool)]
        y = y[np.array(np.abs(y_nan - 1), dtype=bool)]

        # fit_func = np.poly1d(np.polyfit(x, y, 13))
        fit_x = np.arange(0, 50.5, 0.5)
        # fit_y = fit_func(fit_x)
        # fit_y = np.interp(x_sig,x_array,y_array)
        tck = si.splrep(x, y)
        fit_y = si.splev(fit_x, tck)

        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax1.set_title("MTF@150lp/mm vs Distance for " + str(name))
        ax1.set_xlabel('Distance (mm)')
        ax1.set_ylabel('MTF@150lp/mm')

        ax1.plot(x, y, 'ro', c='r', label='Data')
        ax1.plot(fit_x, fit_y, 'b-', c='b', label='Fit')

        x_fit = []
        y_fit = []

        counter = 0
        for i in [3, 7, 25, 50]:
            pos_x = self.findClosest(fit_x, i)
            x_fit.append(fit_x[pos_x])
            y_fit.append(fit_y[pos_x])
            ax1.annotate('x=%.2f, y=%.2f' % (fit_x[pos_x], fit_y[pos_x]), xy=(fit_x[pos_x], fit_y[pos_x]),
                         xytext=(3 + 10 * counter, 20 + 15 * counter),
                         arrowprops=dict(facecolor='gray', shrink=0.05))
            counter += 1

        plt.ylim(0, 200)
        ax1.legend(loc=4)

        fig.savefig(output_path)
        plt.close(fig)

        return x_fit, y_fit      

    def MTF50_fit(self, x, a, b, c):
        return a * np.exp(-1 / (2 * (x - b) ** 2)) / (x - b) ** 2 + c

    def findClosest(self, A, target):
        # A must be sorted
        idx = A.searchsorted(target)
        idx = np.clip(idx, 1, len(A) - 1)
        left = A[idx - 1]
        right = A[idx]
        idx -= target - left < right - target
        return idx

    def __del__(self):
        pass


# Usage
'''
folder = 'C:\\Users\\steven.yampolsky\\Box Sync\\Camera Production Test\\Camera Calibration\\Camera Test v1\\Data\\Toshiba_SN_161128-08-4\\2016-12-19_10-17-53'
MTF(folder)
'''
'''
folder = 'C:\\Users\\steven.yampolsky\\Box Sync\\Camera Production Test\\Camera Calibration\\Data'
Datas = os.listdir(folder)
for i in Datas:
    dates = os.listdir(os.path.join(folder,i))
    for j in dates:
        print(os.path.join(folder,i,j))
        MTF(os.path.join(folder,i,j))
'''
