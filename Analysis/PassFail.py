import pandas as pd
import os
import zipfile
import numpy as np
from colorama import init, Fore, Back, Style
init()

class passfail:
    def __init__(self, path, study, datetime, verbose=False):
        print("\nCompiling Pass/Fail...")

        self.path = path
        self.study = study
        self.datetime = datetime
        self.file = pd.read_csv(os.path.join(self.path, self.study+'_'+self.datetime+'_'+'output.csv'), sep=',', na_values = "NULL")
        self.compile()

        print("\nTest Result Compilation Complete!")

    def power(self):
        spec = 13
        led_power = self.file.loc[self.file['attribute'] == 'led_scope_power_mw']

        led_power = led_power.copy()
        led_power['spec'] = 'greater_than_' + str(spec) + '_mw'
        led_power = led_power.copy()

        led_power['pass'] = float(led_power['value']) >= spec
        
        if float(led_power['value']) < spec:
            print('Optical power output too low')

        return led_power

    def mtf(self):  # and Depth of Field

        spec = 10
        #increased to 10 as initial step to catch bad units

        mtf_df = pd.DataFrame()
        index3 = self.file.loc[self.file['attribute'] == 'mtf_150_lp-mm_3.0_mm']
        mtf_df = mtf_df.append(index3, ignore_index=True)
        index7 = self.file.loc[self.file['attribute'] == 'mtf_150_lp-mm_7.0_mm']
        mtf_df = mtf_df.append(index7, ignore_index=True)
        index25 = self.file.loc[self.file['attribute'] == 'mtf_150_lp-mm_25.0_mm']
        mtf_df = mtf_df.append(index25, ignore_index=True)
        index50 = self.file.loc[self.file['attribute'] == 'mtf_150_lp-mm_50.0_mm']
        mtf_df = mtf_df.append(index50, ignore_index=True)

        mtf_df = mtf_df.copy()
        mtf_df['spec'] = 'greater_than_' + str(spec) + '_percent_at_150_lp-mm'
        mtf_df = mtf_df.copy()
        mtf_df['pass'] = pd.Series(mtf_df['value'], dtype=float) > spec
    
        if False in list(mtf_df['pass']):
            print('MTF is too low')
            
        return mtf_df

    def fov(self):
        spec_l = 110
        spec_u = 130
        fov_ = self.file.loc[self.file['attribute'] == 'field_of_view_degrees']

        fov_ = fov_.copy()
        fov_['spec'] = 'greater_than_' + str(spec_l) + '_degrees' + 'and_less_than_' + str(spec_u) + '_degrees'
        fov_ = fov_.copy()
        fov_['pass'] = float(fov_['value']) >= spec_l  and float(fov_['value']) <= spec_u
        
        if float(fov_['value']) < spec_l:
            print('Field of view is not large enough')
        if float(fov_['value']) > spec_u:
            print('Field of view is too large')

        return fov_

    def pointing(self):
        spec = 5

        pointing_ = self.file.loc[self.file['attribute'] == 'pointing_deviation_degrees']

        pointing_ = pointing_.copy()
        pointing_['spec'] = 'less_than_' + str(spec) + '_degrees'

        pointing_ = pointing_.copy()

        pointing_['pass'] = np.abs(float(pointing_['value'])) <= spec
        
        if float(pointing_['value']) > spec:
            print('Scope pointing is too large')

        return pointing_

    def camera_uniformity(self):
        camera_uniformity_spec = 0.5
        # spec from JAMA
        camera_uniformity_ = self.file.loc[self.file['attribute'] == 'camera_uniformity_roll_off']

        camera_uniformity_ = camera_uniformity_.copy()
        camera_uniformity_['spec'] = 'greater_than_' + str(camera_uniformity_spec) + '_of_peak_luminance'

        camera_uniformity_ = camera_uniformity_.copy()
        camera_uniformity_['pass'] = float(camera_uniformity_['value']) >= camera_uniformity_spec

        if float(camera_uniformity_['value']) < camera_uniformity_spec:
            print('Camera uniformity does not meet spec')
        
        return camera_uniformity_

    def LED_uniformity(self):

        LED_uniformity_spec = 0.2
        # Spec in JAMA is writeen to <80% roll-off, calculation interprets the spec as >20% of peak illuminance

        LED_uniformity_ = self.file.loc[self.file['attribute'] == 'LED_uniformity_roll_off']

        LED_uniformity_ = LED_uniformity_.copy()
        LED_uniformity_['spec'] = 'greater_than_' + str(LED_uniformity_spec) + '_of_peak_luminance'

        LED_uniformity_ = LED_uniformity_.copy()
        LED_uniformity_['pass'] = float(LED_uniformity_['value']) >= LED_uniformity_spec
        
        
        if float(LED_uniformity_['value']) < LED_uniformity_spec:
            print('LED Uniformity does not meet spec')
            
        return LED_uniformity_
    
    def Natural_angle(self):
        #spec set to +/- 60 degrees
        natural_angle_spec = 60
        natural_angle_ = self.file.loc[self.file['attribute'] == 'toshiba_natural_angle']
        
        natural_angle_ = natural_angle_.copy()
        natural_angle_['spec'] = 'absolute_value_less_than_' + str(natural_angle_spec) + '_degrees'

        natural_angle_ = natural_angle_.copy()
        natural_angle_['pass'] = abs(float(natural_angle_['value'])) <= natural_angle_spec
        #this check is due to the possibility of a NaN being present, a more elegant
        #solution could not be determiened at the time
        if (abs(float(natural_angle_['value'])) <= natural_angle_spec) == False:
            print('Natural angle is too large')
        
        return natural_angle_
        

    def compile(self):
        self.output = pd.DataFrame()
        self.output = self.output.append(self.power(), ignore_index=True)
        self.output = self.output.append(self.mtf(), ignore_index=True)
        self.output = self.output.append(self.fov(), ignore_index=True)
        self.output = self.output.append(self.pointing(), ignore_index=True)
        self.output = self.output.append(self.camera_uniformity(), ignore_index=True)
        self.output = self.output.append(self.LED_uniformity(), ignore_index=True)
        self.output = self.output.append(self.Natural_angle(), ignore_index=True)
        self.output = self.output.reindex(columns=['test'] + self.output.columns.tolist())
        self.output['test'] = ['m1', 'm2', 'm4', 'm4', 'm2', 'm3', 'm5', 'm9', 'm10','m11']
        self.output['scope'] = self.study
        self.output['time'] = self.datetime
        self.output = self.output[['scope', 'time', 'test', 'attribute', 'spec', 'value', 'pass']]

        # this is the line that displays the data frame
        # print('\n' + self.output.to_string(index=False) + '\n')

        if False in list(self.output['pass']):
            print(Back.RED + 'The device FAILED, reject the unit!')
            print(Style.RESET_ALL)
        else:
            print(Back.GREEN + 'The device PASSED all tests!')
            print(Style.RESET_ALL)



        self.output.to_csv(os.path.join(self.path, self.study+'_'+self.datetime+'_'+'results.csv'), index=False, na_rep = "NULL")

        # pad output.csv with scope number and time
        if 'date' not in self.file:

            indeces = []
            for i in range(len(self.datetime)):
                if self.datetime[i] == '_':
                    indeces.append(i)

            self.file['serial_number'] = self.study
            self.file['date'] = self.datetime[:indeces[0]] + ' ' + self.datetime[-11:-9] + ':' + self.datetime[
                                                                                                 -8:-6] + ':' + self.datetime[
                                                                                                                -5:-3]
            self.file.to_csv(os.path.join(self.path, self.study+'_'+self.datetime+'_'+'output.csv'), index=False, na_rep = "NULL")

# Usage
#derp = passfail('D:\\Toshiba_SN_170328-8-1\\2017-05-16_17-56-39', 'ToshibaBananas', '12-12-12')
