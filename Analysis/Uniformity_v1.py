# -*- coding: utf-8 -*-
"""
Created on Thu May 18 12:44:43 2017

@author: Jonathan.Brand
"""

import numpy as np
import cv2
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import os
import pandas as pd


class Uniformity_analysis:
    def __init__(self, path, study, datetime, verbose=False):
        print("\nStarting Uniformity Analysis...")
        self.study = study
        self.datetime = datetime
        # sets path to save uniformity analysis data
        self.path = path
        if not os.path.exists(os.path.join(self.path, '4_uniformity')):
            os.mkdir(os.path.join(self.path, '4_uniformity'))
        self.save_path = os.path.join(self.path, '4_uniformity')
        # the path where the uniformity data are
        self.uniformity_path = os.path.join(self.path, 'g_uniformity')

        # The image f_scope led_camera
        self.file = 'uniformity_led.bmp'
        self.led_camera_img = cv2.imread(os.path.join(self.uniformity_path, self.file), 0)
        # The image that defines the camera uniformity (we assume the target is close to uniform)
        self.file = 'uniformity_camera.bmp'
        self.camera_img = cv2.imread(os.path.join(self.uniformity_path, self.file), 0)
        self.data_path = os.path.join(self.path, self.study + '_' + self.datetime + '_' + 'output.csv')

        # extract the union jack sample of the uniformity
        self.camera_uniformity_data = self.collect_uniformity(self.camera_img, verbose)
        self.led_camera_uniformity_data = self.collect_uniformity(self.led_camera_img, verbose)

        # isolate the LED illumination by performing f_scopeled_camera/f_camera
        self.led_uniformity_data = self.correct_for_camera(self.camera_uniformity_data, self.led_camera_uniformity_data,
                                                           verbose)
        # analyze the uniformity data

        # These sequences analyze the Uniformity Data for the three images
        self.camera_unif_fig, self.camera_unif_data = self.analyze_uniformity(self.camera_uniformity_data, verbose)
        self.camera_unif_fig.savefig(
            self.save_path + '/camera_uniformity_plot.png')  # , bbox_extra_artists=(lgd,), bbox_inches='tight')
        plt.clf()
        self.camera_unif_data.to_csv(self.save_path + '/camera_uniformity_data.csv')

        self.LED_unif_fig, self.LED_unif_data = self.analyze_uniformity(self.led_uniformity_data, verbose)
        self.LED_unif_fig.savefig(
            self.save_path + '/LED_uniformity_plot.png')  # , bbox_extra_artists=(lgd,), bbox_inches='tight')
        plt.clf()
        self.LED_unif_data.to_csv(self.save_path + '/LED_uniformity_data.csv')

        self.LED_camera_unif_fig, self.LED_camera_unif_data = self.analyze_uniformity(self.led_camera_uniformity_data,
                                                                                      verbose)
        self.LED_camera_unif_fig.savefig(
            self.save_path + '/LED_camera_uniformity_plot.png')  # , bbox_extra_artists=(lgd,), bbox_inches='tight')
        plt.clf()
        self.LED_camera_unif_data.to_csv(self.save_path + '/LED_camera_uniformity_data.csv')

        LED_uniformity_eval = self.LED_unif_data.values
        camera_uniformity_eval = self.camera_unif_data.values

        LED_r_squared = LED_uniformity_eval[0, :4]
        LED_unif_roll_off = LED_uniformity_eval[0, 4:]

        camera_r_squared = camera_uniformity_eval[0, :4]
        camera_unif_roll_off = camera_uniformity_eval[0, 4:]

        if verbose:
            print(np.round(np.min(LED_unif_roll_off), 2))

        print("-(1) Uniformity Analysis Complete!")

        self.entry = ['camera_uniformity_roll_off', 'camera_uniformity_r_squared', 'LED_uniformity_roll_off',
                      'LED_uniformity_r_squared']
        self.data = [np.round(np.min(camera_unif_roll_off), 2), np.round(np.min(camera_r_squared), 2),
                     np.round(np.min(LED_unif_roll_off), 2), np.round(np.min(LED_r_squared), 2)]
        self.dataFrame_add = pd.DataFrame({'value': self.data, 'attribute': self.entry})
        self.dataFrame = pd.read_csv(self.data_path)
        self.data_to_save = pd.concat([self.dataFrame, self.dataFrame_add], ignore_index=True)
        self.data_to_save.to_csv(os.path.join(self.path, self.study + '_' + self.datetime + '_' + 'output.csv'),
                                 index=False)

    def collect_uniformity(self, img, verbose=False):
        # collect the center horizontal line and normalize to the max
        # a range of5:215 in the horizontal and vertical eliminates edge erros
        # a range of 31:179 on the diagonals is the equivilant euclidea distance 
        # to the horizontl and vertical samples
        img = img / np.amax(img)
        horizontal_line = np.mean(img[110:112, :], axis=0)
        # horizontal_line = horizontal_line/np.max(horizontal_line[5:215])
        # collect and normalize the left-right diagonal
        diagonal_lr = np.diagonal(img)
        # diagonal_lr = diagonal_lr/np.max(diagonal_lr[31:179])

        img_rot = np.rot90(img)

        # collect the center vertical line and normalize to the max
        vertical_line = np.mean(img_rot[110:112, :], axis=0)
        # vertical_line = vertical_line/np.max(vertical_line[5:215])

        # collect and normalize the right-left diagonal
        diagonal_rl = np.diagonal(img_rot)

        # diagonal_rl = diagonal_rl/np.max(diagonal_rl[31:179])

        data = {'Horizontal Sample': horizontal_line, 'Vertical Sample': vertical_line, 'Diagonal L-R': diagonal_lr,
                'Diagonal R-L': diagonal_rl}
        df = pd.DataFrame(data, columns=['Horizontal Sample', 'Vertical Sample', 'Diagonal L-R', 'Diagonal R-L'])
        return df

    def correct_for_camera(self, camera_uniformity_data, led_camera_uniformity_data, verbose):
        camera_data = self.camera_uniformity_data.values
        led_camera_data = self.led_camera_uniformity_data.values

        # recover LED uniformity for f_camera_LED/f_camera
        horizontal_line = led_camera_data[:, 0] / camera_data[:, 0]
        vertical_line = led_camera_data[:, 1] / camera_data[:, 1]
        diagonal_lr = led_camera_data[:, 2] / camera_data[:, 2]
        diagonal_rl = led_camera_data[:, 3] / camera_data[:, 3]

        # A re-normalization is performed to correct for noise at the maximum of the camera image
        horizontal_line = horizontal_line / np.max(horizontal_line)
        vertical_line = vertical_line / np.max(vertical_line)
        diagonal_lr = diagonal_lr / np.max(diagonal_lr)
        diagonal_rl = diagonal_rl / np.max(diagonal_rl)

        data = {'Horizonatal Sample': horizontal_line, 'Vertical Sample': vertical_line, 'Digonal L-R': diagonal_lr,
                'Digonal R-L': diagonal_rl}
        df = pd.DataFrame(data, columns=['Horizonatal Sample', 'Vertical Sample', 'Digonal L-R', 'Digonal R-L'])

        return df

    def analyze_uniformity(self, uniformity_data, verbose=False):
        data = uniformity_data.values

        # get data
        horizontal_data = data[5:215, 0]
        vertical_data = data[5:215, 1]
        diagonal_lr_data = data[31:179, 2]
        diagonal_rl_data = data[31:179, 3]
        # define the gauss function to fit to
        p0 = [1., 105., 1.]

        def gauss(x, *p):
            A, mu, sigma = p
            return A * np.exp(-(x - mu) ** 2 / (2. * sigma ** 2))

        # Horizontal and vertical pixel splace definition
        pixel = range(210)
        # diagonal pixel space definition
        d_pixel = np.linspace(0, np.sqrt(2) * 148, num=148)
        # fit the gauss variable based on the input data
        coeff_hor, var_matrix_hor = curve_fit(gauss, pixel, horizontal_data, p0=p0)
        coeff_ver, var_matrix_ver = curve_fit(gauss, pixel, vertical_data, p0=p0)
        coeff_dlr, var_matrix_dlr = curve_fit(gauss, d_pixel, diagonal_lr_data, p0=p0)
        coeff_drl, var_matrix_drl = curve_fit(gauss, d_pixel, diagonal_rl_data, p0=p0)
        # create the fitted data vectors
        hor_fit = gauss(pixel, *coeff_hor)
        ver_fit = gauss(pixel, *coeff_ver)
        dlr_fit = gauss(d_pixel, *coeff_dlr)
        drl_fit = gauss(d_pixel, *coeff_drl)
        # calcualte total sum of squares
        ss_tot_hor = np.sum((horizontal_data - np.mean(horizontal_data)) ** 2)
        ss_tot_ver = np.sum((vertical_data - np.mean(vertical_data)) ** 2)
        ss_tot_dlr = np.sum((diagonal_lr_data - np.mean(diagonal_lr_data)) ** 2)
        ss_tot_drl = np.sum((diagonal_rl_data - np.mean(diagonal_rl_data)) ** 2)
        # calcualte sum of squares residuals
        ss_res_hor = np.sum((horizontal_data - hor_fit) ** 2)
        ss_res_ver = np.sum((vertical_data - ver_fit) ** 2)
        ss_res_dlr = np.sum((diagonal_lr_data - dlr_fit) ** 2)
        ss_res_drl = np.sum((diagonal_rl_data - drl_fit) ** 2)
        # Calculate R^2
        r_sq_hor = 1 - (ss_res_hor / ss_tot_hor)
        r_sq_ver = 1 - (ss_res_ver / ss_tot_ver)
        r_sq_dlr = 1 - (ss_res_dlr / ss_tot_dlr)
        r_sq_drl = 1 - (ss_res_drl / ss_tot_drl)
        # Create figures to be saved
        fig = plt.figure(1)
        ax = fig.add_subplot(111)
        ax.plot(pixel, horizontal_data, 'r--', label='Horizontal')
        ax.plot(pixel, vertical_data, 'g--', label='Vertical')
        ax.plot(d_pixel, diagonal_lr_data, 'b--', label='Diagonal LR')
        ax.plot(d_pixel, diagonal_rl_data, 'm--', label='Diagonal RL')
        plt.ylabel('Normalized Intensity')
        plt.xlabel('Horizontal Pixel Dimension')
        plt.legend(bbox_to_anchor=None, loc=8, borderaxespad=0.)
        plt.ylim(0, 1.05)
        plt.xlim(0, 200)
        ax.grid('on')
        # plt.show()
        '''
        data = {'Horizonatal R^2': [r_sq_hor],'Vertical R^2': [r_sq_ver], 
        'DLR R^2': [r_sq_dlr],'DRL R^2': [r_sq_drl],'Horizonatal unif': [1-(np.max(horizontal_data)-np.min(horizontal_data))],
        'Vertical unif': [1-(np.max(vertical_data)-np.min(vertical_data))],
        'DLR unif': [1-(np.max(diagonal_lr_data)-np.min(diagonal_lr_data))],
        'DRL unif': [1-(np.max(diagonal_rl_data)-np.min(diagonal_rl_data))]}
        '''
        data = {'Horizonatal R^2': [r_sq_hor], 'Vertical R^2': [r_sq_ver],
                'DLR R^2': [r_sq_dlr], 'DRL R^2': [r_sq_drl], 'Horizonatal unif': [np.min(horizontal_data)],
                'Vertical unif': [np.min(vertical_data)],
                'DLR unif': [np.min(diagonal_lr_data)],
                'DRL unif': [np.min(diagonal_rl_data)]}
        df = pd.DataFrame(data, columns=['Horizonatal R^2', 'Vertical R^2', 'DLR R^2', 'DRL R^2', 'Horizonatal unif',
                                         'Vertical unif', 'DLR unif', 'DRL unif'])

        return fig, df
