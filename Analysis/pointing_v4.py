# -*- coding: utf-8 -*-
"""
Created on Fri Oct 13 13:18:35 2017

@author: Jonathan.Brand
"""

import numpy as np
import cv2
import matplotlib.pyplot as plt
import scipy.optimize as optimization
import os
import pandas as pd

class pointing_v4:
    def __init__(self, path, study, datetime, verbose = False):
        
        print("Starting Pointing analysis...")

        self.study = study
        self.datetime = datetime
        self.path = path
        
        self.data_path = os.path.join(self.path, self.study+'_'+self.datetime+'_'+'output.csv')
        Z = [18.8515625, 23.8125, 33.734375, 53.578125]
        
        if not os.path.exists(os.path.join(self.path, '2_fov_and_pointing')):
            os.mkdir(os.path.join(self.path, '2_fov_and_pointing'))
        if not os.path.exists(os.path.join(self.path, '2_fov_and_pointing','pointing')):
            os.mkdir(os.path.join(self.path, '2_fov_and_pointing','pointing'))
        self.save_path = os.path.join(self.path, '2_fov_and_pointing','pointing')
        
        
        print('\nStarting pointing analysis...')
        self.x_DOV_fit, self.y_DOV_fit, self.pointing = self.estimate_DOV(self.path, verbose)
        #print('Pointing = '+str(np.round(self.pointing,2))+' degrees')
        #print('DOV  = ' + str(self.pointing)+' degrees')
        #entry = ['a_x', 'b_x','a_y','b_y', 'fov_1', 'fov_2', 'fov_3','fov_4', 'pointing']
        entry = ['pointing']
        #data = [self.x_DOV_fit[0], self.x_DOV_fit[1],self.y_DOV_fit[0], self.y_DOV_fit[1], self.fov_1, self.fov_2, self.fov_3, self.fov_4, self.pointing]
        data = [self.pointing]        
        dataFrame = pd.DataFrame({'value': data, 'attribute': entry})
        dataFrame.to_csv(os.path.join(self.save_path, 'pointing_data.csv'), index = False)
        
        self.entry = ['pointing_deviation_degrees']
        self.data = [np.round(self.pointing,2)]
        self.dataFrame_add = pd.DataFrame({'value': self.data, 'attribute': self.entry})
        self.dataFrame = pd.read_csv(self.data_path)
        self.data_to_save = pd.concat([self.dataFrame, self.dataFrame_add], ignore_index=True)
        self.data_to_save.to_csv(os.path.join(self.path, self.study+'_'+self.datetime+'_'+'output.csv'), index=False)
        
    def estimate_DOV(self, path, verbose):
        
        #mtf_path = os.path.join(path,'c_mtf')
        z_dist = [10,14,18,22,26,30,34,38,42,46,50]
        
        centers_col = []
        centers_row = []
        
        for idm,m in enumerate(z_dist):
            if len(str(m)) == 1:
                img_str = '0'+str(m)+'.0_mm.bmp'
            else:
                img_str = str(m)+'.0_mm.bmp'
            img = cv2.imread(os.path.join(path,'c_mtf',img_str))
            col, row = self.find_center(img, idm, verbose)
            centers_col.append(col-110)
            centers_row.append(row-110)
        
        
        if verbose:
            print('x image height (pixels): '+str(centers_col))
            print('y image height (pixels): '+str(centers_row))
            
        pix_pitch = 0.00175   
        center_x_img = np.asarray(centers_col)*pix_pitch
        center_y_img = np.asarray(centers_row)*pix_pitch
        
        if verbose:
            print('x image height (mm): '+str(center_x_img))
            print('y image height (mm): '+str(center_y_img))
        
        lens_efl = 0.32
        
        z_prime = 1/((1/np.negative(z_dist)) + (1/lens_efl))
        
        #m = z_prime/np.negative(z_dist)
        m = 1/(1+(np.array(z_dist)/lens_efl))
        
        center_x_obj = center_x_img/m
        center_y_obj = center_y_img/m
        
        if verbose:
            print('x object height : '+str(center_x_obj))
            print('y object height : '+str(center_y_obj))
        
        center_x_err = center_x_obj
        center_y_err = center_y_obj
        
        if verbose:
            print('x object error : ' +str(center_x_err))
            print('y object error : ' +str(center_y_err))
        
            
        def lin_func(x,a,b):
            return  a*x+b
        #the DOV fit is performed with a parametric form of a linear 3D vector f(z) = x, f(z) = y
        DOV_a_x, DOV_x_cov = optimization.curve_fit(lin_func, z_dist, center_x_err)
        DOV_a_y, DOV_y_cov = optimization.curve_fit(lin_func, z_dist, center_y_err)
        fit_func_x = np.poly1d(DOV_a_x)
        fit_func_y = np.poly1d(DOV_a_y)
        
        fit_x = np.arange(2, 51, 0.5)
        fit_y = fit_func_x(fit_x)
        fig = plt.figure()
        fig.canvas.set_window_title('x data fit ' )
        
        plt.scatter(z_dist, center_x_err)
        plt.plot(fit_x, fit_y, 'r-')
        plt.xlim(0, 51)
        fig.savefig(os.path.join(self.save_path, 'X_center_fit.png'), bbox_inches='tight')
        if verbose:
            plt.show()
        else:
            plt.close(fig)     
        
        fit_x = np.arange(2, 51, 0.5)
        fit_y = fit_func_y(fit_x)
        fig = plt.figure()
        fig.canvas.set_window_title('y data fit ' )
        
        plt.scatter(z_dist, center_y_err)
        plt.plot(fit_x, fit_y, 'r-')
        plt.xlim(0, 51)
        fig.savefig(os.path.join(self.save_path, 'Y_center_fit.png'), bbox_inches='tight')
        if verbose:
            plt.show()
        else:
            plt.close(fig)     
        
        x_0 = fit_func_x(z_dist[0])
        y_0 = fit_func_y(z_dist[0])
        
        x_1 = fit_func_x(z_dist[-1])
        y_1 = fit_func_y(z_dist[-1])
        
        Y_p = np.array(y_1-y_0)
        
        Z_p = np.sqrt(np.power((z_dist[-1]-z_dist[0]),2)+np.power((x_1-x_0),2))
        
        
        DOV = 180*np.arctan(Y_p/Z_p)/np.pi
        
        return DOV_a_x, DOV_a_y, DOV
        
    def find_center(self,img,idm,verbose):
        
        if len(img.shape) == 3:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        
        img_mean = np.mean(img)
        anal_img = img.copy()
        for i in range(len(anal_img)):
            for j in range(len(anal_img[i])):
                if anal_img[i, j] > img_mean:
                    anal_img[i, j] = 255
                else:
                    anal_img[i, j] = 0


        fig = plt.figure()
        fig.canvas.set_window_title('Original_'+str(idm+1))
        plt.imshow(img)
        fig.savefig(os.path.join(self.save_path, 'original_'+str(idm+1)+'.png'), bbox_inches='tight')
        if verbose:
            plt.show()
        else:
            plt.close(fig)

        fig = plt.figure()
        fig.canvas.set_window_title('Polarized Image_'+str(idm+1))
        plt.imshow(anal_img)
        fig.savefig(os.path.join(self.save_path, 'polarized_'+str(idm+1)+'.png'), bbox_inches='tight')
        if verbose:
            plt.show()
        else:
            plt.close(fig)

        edges = img.copy()

        edges = cv2.Canny(edges, 50, 150, apertureSize=3)

        fig = plt.figure()
        fig.canvas.set_window_title('Edges_'+str(idm+1))
        plt.imshow(edges)
        fig.savefig(os.path.join(self.save_path, 'edges_'+str(idm+1)+'.png'), bbox_inches='tight')
        if verbose:
            plt.show()
        else:
            plt.close(fig)

        # Search for line using Hough, Find EXACTLY 4 line sets
        j = 70
        num_lines = 4
        while True:

            lines = cv2.HoughLines(edges, 1, np.pi / 360, j, max_theta=np.pi)
            lines = lines.reshape(len(lines), 2)
            lines = lines[lines[:, 1].argsort()]

            angles = np.array(lines).T[1]
            split = []
            for i in range(len(angles) - 1):
                # If change is greater than ~30 deg
                if angles[i + 1] - angles[i] > 0.5:
                    split.append(i + 1)

            if len(split) + 1 > num_lines:
                j += 10
            elif j == 0:
                break
            elif len(split) + 1 < num_lines:
                j -= 10
            else:
                break

        # get X,Y for lines found
        lines_pts = []
        for rho, theta in lines:
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a * rho
            y0 = b * rho
            x1 = int(x0 + 1000 * (-b))
            y1 = int(y0 + 1000 * (a))
            x2 = int(x0 - 1000 * (-b))
            y2 = int(y0 - 1000 * (a))
            lines_pts.append([x1, y1, x2, y2])

        # Separate lines by split
        lines_list = []
        for i in range(num_lines):
            if i == 0:
                lines_list.append(lines_pts[0:split[0]])
            elif not i == num_lines - 1:
                lines_list.append(lines_pts[split[i - 1]:split[i]])
            else:
                lines_list.append(lines_pts[split[i - 1]:])

        # Find Intersections
        sects = []
        for i in lines_list:
            for j in lines_list[lines_list.index(i) + 1:]:
                for k in i:
                    for l in j:
                        k = np.array(k)
                        l = np.array(l)
                        sects.append(self.seg_intersect(k[0:2], k[2:], l[0:2], l[2:]))

        # Calculate Center from Intersects
        center = np.array(np.round(np.nanmean(sects, axis=0)), dtype=np.int)
        #return row and column separatly
        return center[0],center[1]
        
    def seg_intersect(self, a1, a2, b1, b2):
        da = a2 - a1
        db = b2 - b1
        dp = a1 - b1
        dap = self.perp(da)
        denom = np.dot(dap, db)
        num = np.dot(dap, dp)
        return (num / denom.astype(float)) * db + b1
        
    def perp(self, a):
        b = np.empty_like(a)
        b[0] = -a[1]
        b[1] = a[0]
        return b
        
    def __del__(self):
        pass