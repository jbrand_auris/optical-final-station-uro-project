import os
import zipfile
import shutil
import datetime
import dateutil.parser
import boto3
import pandas as pd
import numpy as np


class admin:
    def __init__(self, path, study, datetime):

        self.path = path
        self.datetime = datetime
        self.study = study

    def manifest(self):
        print("\nCreating manifest...")
        startpath = self.path
        manifest = open(os.path.join(self.path, 'manifest.txt'), 'w+')
        manifest.write(self.study + '/\n')
        for root, dirs, files in os.walk(startpath):
            level = root.replace(startpath, '').count(os.sep)
            indent = '|---' * (level + 1)
            manifest.write('{}{}/'.format(indent, os.path.basename(root)) + '\n')
            # print('{}{}/'.format(indent, os.path.basename(root)))
            subindent = '|---' * (level + 2)
            for f in files:
                # print('{}{}'.format(subindent, f))
                manifest.write('{}{}'.format(subindent, f) + '\n')
        manifest.close()

    def zip(self):
        print("\nZipping Data and Results...")
        self.zip_fn = os.path.join(self.path, self.datetime + '_' + self.study + '.zip')
        self.zipdir(self.path, self.zip_fn)

    def zipdir(self, dirPath=None, zipFilePath=None, includeDirInZip=False):
        """Create a zip archive from a directory.

        Note that this function is designed to put files in the zip archive with
        either no parent directory or just one parent directory, so it will trim any
        leading directories in the filesystem paths and not include them inside the
        zip archive paths. This is generally the case when you want to just take a
        directory and make it into a zip file that can be extracted in different
        locations. 

        Keyword arguments:

        dirPath -- string path to the directory to archive. This is the only
        required argument. It can be absolute or relative, but only one or zero
        leading directories will be included in the zip archive.

        zipFilePath -- string path to the output zip file. This can be an absolute
        or relative path. If the zip file already exists, it will be updated. If
        not, it will be created. If you want to replace it from scratch, delete it
        prior to calling this function. (default is computed as dirPath + ".zip")

        includeDirInZip -- boolean indicating whether the top level directory should
        be included in the archive or omitted. (default True)

    """
        if not zipFilePath:
            zipFilePath = dirPath + ".zip"
        if not os.path.isdir(dirPath):
            raise OSError("dirPath argument must point to a directory. "
                          "'%s' does not." % dirPath)
        parentDir, dirToZip = os.path.split(dirPath)

        # Little nested function to prepare the proper archive path
        def trimPath(path):
            archivePath = path.replace(parentDir, "", 1)
            if parentDir:
                archivePath = archivePath.replace(os.path.sep, "", 1)
            if not includeDirInZip:
                archivePath = archivePath.replace(dirToZip + os.path.sep, "", 1)
            return os.path.normcase(archivePath)

        outFile = zipfile.ZipFile(zipFilePath, "w",
                                  compression=zipfile.ZIP_DEFLATED)
        for (archiveDirPath, dirNames, fileNames) in os.walk(dirPath):
            for fileName in fileNames:
                filePath = os.path.join(archiveDirPath, fileName)
                outFile.write(filePath, trimPath(filePath))
            # Make sure we get empty directories as well
            if not fileNames and not dirNames:
                zipInfo = zipfile.ZipInfo(trimPath(archiveDirPath) + "/")
                # some web sites suggest doing
                # zipInfo.external_attr = 16
                # or
                # zipInfo.external_attr = 48
                # Here to allow for inserting an empty directory.  Still TBD/TODO.
                outFile.writestr(zipInfo, "")
        outFile.close()

    def upload(self):
        print('\nUploading...')
        s3 = boto3.client('s3', aws_access_key_id='AKIAIKLEP4YL3ELHT4TQ',
                          aws_secret_access_key='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')
        s3.upload_file(self.zip_fn, 'cameracaldata', self.datetime + '_' +
        self.study + '.zip')
        print('Upload Complete!')

    def delete_30_day_old_data(self):
        print('\nDeleting data older than 30 days...')

        # Currently in folder first by serial then by date, must go up 2 levels to see all data!
        os.chdir('..')
        os.chdir('..')
        data_dir = os.getcwd()
        for m in os.listdir(data_dir):
            # delete folders 30 days old
            for n in os.listdir(os.path.join(data_dir, m)):
                # For future code, when output.csv has sn and date encoded into filename
                '''
                date = pd.read_csv(os.path.join(data_dir, m, n, m + '_' + n + '_' + 'output.csv'))[
                    'date'][
                    0]
                folder_date = dateutil.parser.parse(date)
                '''
                folder_date = datetime.datetime(np.int(n[0:4]), np.int(n[5:7]), np.int(n[8:10]), 0, 0)
                if (datetime.datetime.now() - folder_date).total_seconds() > 5184000:
                    shutil.rmtree(os.path.join(data_dir, m, n))
                    # delete empty folders
        if os.listdir(os.path.join(data_dir, m)) == []:
            shutil.rmtree(os.path.join(data_dir, m))
        print('Deletion Completed!')
